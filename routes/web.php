<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SubCategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\TemplateController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\SubscriberController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\UnitController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

/*-------------------------front routes-----------------------*/
Route::group(['namespace' => 'Front'], function () {
	Route::get('/', [HomeController::class, 'index'])->name('front.home.index');
	Route::any('/subscribe', [HomeController::class, 'subscribe'])->name('front.subscribe');
	Route::get('/clear-cache', function () {
		$exitCode = Artisan::call('cache:clear');
		$exitCode = Artisan::call('config:cache');
		echo "cache alera";
		die;
		// return what you want
	});
});
/*-------------------------admin routes-----------------------*/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
	Route::any('/', [LoginController::class, 'login'])->name('admin.login');
	Route::any('/login', [LoginController::class, 'login'])->name('admin.login');
	Route::middleware(['admin'])->group(function () {
		Route::any('/dashboard', [UsersController::class, 'dashboard'])->name('admin.dashboard');
		// user routes
		Route::any('users', [UsersController::class, 'index'])->name('admin.users.index');
		Route::any('users/datatables', [UsersController::class, 'datatables'])->name('admin.users.datatables');
		Route::any('/user/add/{id?}', [UsersController::class, 'add'])->name('admin.users.add');
		Route::any('/user/view/{id}', [UsersController::class, 'view'])->name('admin.users.view');
		Route::any('/user/status', [UsersController::class, 'status'])->name('admin.users.status');
		Route::any('/user/delete', [UsersController::class, 'view'])->name('admin.users.delete');
		Route::any('/user/profile', [UsersController::class, 'profile'])->name('admin.profile');
		Route::any('/user/change-password', [UsersController::class, 'changePassword'])->name('admin.change-password');

		// category routes
		Route::any('category', [CategoryController::class, 'index'])->name('admin.category.index');
		Route::any('category/datatables', [CategoryController::class, 'datatables'])->name('admin.category.datatables');
		Route::any('/category/add/{id?}', [CategoryController::class, 'add'])->name('admin.category.add');
		Route::any('/category/status', [CategoryController::class, 'status'])->name('admin.category.status');
		Route::any('/category/delete', [CategoryController::class, 'delete'])->name('admin.category.delete');

		// sub-category routes
		Route::any('sub-category', [SubCategoryController::class, 'index'])->name('admin.sub-category.index');
		Route::any('sub-category/datatables', [SubCategoryController::class, 'datatables'])->name('admin.sub-category.datatables');
		Route::any('/sub-category/add/{id?}', [SubCategoryController::class, 'add'])->name('admin.sub-category.add');
		Route::any('/sub-category/status', [SubCategoryController::class, 'status'])->name('admin.sub-category.status');
		Route::any('/sub-category/delete', [SubCategoryController::class, 'delete'])->name('admin.sub-category.delete');

		// product routes
		Route::any('product', [ProductController::class, 'index'])->name('admin.product.index');
		Route::any('product/datatables', [ProductController::class, 'datatables'])->name('admin.product.datatables');
		Route::any('/product/add/{id?}', [ProductController::class, 'add'])->name('admin.product.add');
		Route::any('/product/status', [ProductController::class, 'status'])->name('admin.product.status');
		Route::any('/product/subcat', [ProductController::class, 'subcat'])->name('admin.product.subcat');
		Route::any('/product/delete', [ProductController::class, 'delete'])->name('admin.product.delete');

		// invoice routes
		Route::any('order', [OrderController::class, 'index'])->name('admin.order.index');
		Route::any('order/datatables', [OrderController::class, 'datatables'])->name('admin.order.datatables');
		Route::any('/order/add/{id?}', [OrderController::class, 'add'])->name('admin.order.add');
		Route::any('/order/status', [OrderController::class, 'status'])->name('admin.order.status');
		Route::any('/order/subcat', [OrderController::class, 'subcat'])->name('admin.order.subcat');
		Route::any('/order/product', [OrderController::class, 'product'])->name('admin.order.product');
		Route::any('/order/product-price', [OrderController::class, 'productPrice'])->name('admin.order.product-price');
		Route::any('/order/product-weight', [OrderController::class, 'productWeight'])->name('admin.order.product-wight');
		Route::any('/order/view/{id}', [OrderController::class, 'view'])->name('admin.order.view');
		Route::any('/order/delete/{id}', [OrderController::class, 'delete'])->name('admin.order.delete');
		Route::any('/order/download/{id}', [OrderController::class, 'downloadfile'])->name('admin.invoice.download');
		Route::any('/order/print/{id}', [OrderController::class, 'printfile'])->name('admin.invoice.print');

		/*invoice route*/
		Route::any('order/invoice/{id?}', [OrderController::class, 'invoice'])->name('admin.order.invoice');
		// Email template routes
		Route::any('template', [TemplateController::class, 'index'])->name('admin.template.index');
		Route::any('template/datatables', [TemplateController::class, 'datatables'])->name('admin.template.datatables');
		Route::any('/template/add/{id?}', [TemplateController::class, 'add'])->name('admin.template.add');
		Route::any('/template/status', [TemplateController::class, 'status'])->name('admin.template.status');

		// subscriber routes
		Route::any('subscriber', [SubscriberController::class, 'index'])->name('admin.subscriber.index');
		Route::any('subscriber/datatables', [SubscriberController::class, 'datatables'])->name('admin.subscriber.datatables');
		Route::any('/subscriber/status', [SubscriberController::class, 'status'])->name('admin.subscriber.status');
		Route::any('/subscriber/delete/{id?}', [SubscriberController::class, 'delete'])->name('admin.subscriber.delete');

		// page routes
		Route::any('page', [PageController::class, 'index'])->name('admin.page.index');
		Route::any('page/datatables', [PageController::class, 'datatables'])->name('admin.page.datatables');
		Route::any('/page/add/{id?}', [PageController::class, 'add'])->name('admin.page.add');
		Route::any('/page/view/{id}', [PageController::class, 'view'])->name('admin.page.view');
		Route::any('/page/status', [PageController::class, 'status'])->name('admin.page.status');
		Route::any('/page/delete/{id?}', [PageController::class, 'delete'])->name('admin.page.delete');

		// faq routes
		Route::any('faq', [FaqController::class, 'index'])->name('admin.faq.index');
		Route::any('faq/datatables', [FaqController::class, 'datatables'])->name('admin.faq.datatables');
		Route::any('/faq/add/{id?}', [FaqController::class, 'add'])->name('admin.faq.add');
		Route::any('/faq/view/{id}', [FaqController::class, 'view'])->name('admin.faq.view');
		Route::any('/faq/status', [FaqController::class, 'status'])->name('admin.faq.status');
		Route::any('/faq/delete/{id?}', [FaqController::class, 'delete'])->name('admin.faq.delete');

		// unit routes
		Route::any('unit', [UnitController::class, 'index'])->name('admin.unit.index');
		Route::any('unit/datatables', [UnitController::class, 'datatables'])->name('admin.unit.datatables');
		Route::any('/unit/add/{id?}', [UnitController::class, 'add'])->name('admin.unit.add');
		Route::any('/unit/status', [UnitController::class, 'status'])->name('admin.unit.status');
		Route::any('/unit/delete', [UnitController::class, 'delete'])->name('admin.unit.delete');



		// site setting routes
		// site setting route
		//  Route::any('site-setting/{id?}', 'SettingController@add')->name('admin.site-setting');
		Route::any('/site-setting', [SettingController::class, 'index'])->name('admin.settings.index');
		Route::any('site-setting/datatables', [SettingController::class, 'datatables'])->name('admin.settings.datatables');
		Route::any('/site-setting/add/{id?}', [SettingController::class, 'add'])->name('admin.settings.add');
		Route::any('/app-setting', [UsersController::class, 'profile'])->name('admin.app-setting');
		Route::any('/logout', [LoginController::class, 'logout'])->name('admin.logout');
	});
});
