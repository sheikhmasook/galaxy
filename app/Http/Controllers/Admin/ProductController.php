<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Product;
use App\Models\Category;
use App\Models\Unit;
use App\Models\SubCategory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DataTables;
use App\Lib\Helper;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
  public function index()
  {
    $page_title = 'Product List';
    return view('admin.product.index', compact('page_title'));
  }
  public function add(Request $request, $id = null)
  {
    $page_title = ($id) ? 'Edit Product' : 'Add Product';
    $getproduct = Product::find($id);
    $breadcrumbs = [
      ['name' => 'Product', 'relation' => 'link', 'url' => route('admin.product.index')],
      ['name' => $page_title, 'relation' => 'Current', 'url' => ''],
    ];
    if ($request->isMethod('post')) {
      $postdata  = $request->all();
      $rules = [
        'title' => 'required|string|min:2|max:100|unique:category,title,' . $id,
        'category_id' => 'required',
        'sub_cat_id' => 'required',
        'colour' => 'required',
        'units' => 'required',
        'price' => 'required',
        'description' => 'nullable',
      ];

      $validator = Validator::make($postdata, $rules);
      if ($validator->fails()) {
        return response()->json(array('errors' => $validator->messages()), 422);
      } else {
        if (!empty($request->hasFile('product_image'))) {
          $img = $request->file('product_image');
          $file_ext = $img->getClientOriginalExtension();
          $filename = time() . '.' . $file_ext;
          $destination_path = 'uploads/product/' . $filename;
          if (Storage::disk('public')->put($destination_path, file_get_contents($img->getRealPath()))) {
            $postdata['product_image'] = 'public/' . $destination_path;
          }
        }
        //return $postdata;
        if ($id) {
          $getproduct->update($postdata);
          return ['status' => 'true', 'message' => 'Product Update Successfully.'];
        } else {
          Product::create($postdata);
          return ['status' => 'true', 'message' => 'Product Create Successfully.'];
        }
      }
    }
    $category_list = Category::pluck('title', 'id');
    $unit_list = Unit::pluck('title', 'title');
    $sub_category_list = '';
    if ($id) {
      $sub_category_list = SubCategory::where(['id' => $getproduct->sub_cat_id])->pluck('title', 'id');
    }
    // $sub_category_list = SubCategory::pluck('title','id');
    return view('admin.product.add', compact('getproduct', 'page_title', 'id', 'breadcrumbs', 'category_list', 'sub_category_list', 'unit_list'));
  }

  public function datatables()
  {
    $product = Product::where(['status' => '1'])->with(['category', 'subcategory'])
      ->select(['id', 'category_id', 'sub_cat_id', 'title', 'status', 'created_at'])
      ->get();
    return DataTables::of($product)
      ->addColumn('action', function ($product) {
        return '<a title="Edit" href="' . route('admin.product.add', $product->id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>&nbsp;
        <a title="View" href="javascript:void(0)" class="btn btn-xs btn-danger" id="delete_' . $product->id . '" onclick="deleteuser(' . $product->id . ')")><i class="fas fa-trash" ></i></a>';
      })

      ->editColumn('status', function ($product) {
        return Helper::getStatus($product->status, $product->id, route('admin.product.status'));
      })
      ->editColumn('created_at', function ($product) {
        return date("d M-Y", strtotime($product->created_at));
      })
      ->editColumn('title', function ($product) {
        return $product->title;
      })
      ->editColumn('subcategory', function ($product) {
        return $product->subcategory->title;
      })
      ->editColumn('category', function ($product) {
        return $product->category->title;
      })
      ->rawColumns(['status', 'action', 'title', 'created_at', 'category', 'subcategory'])
      ->make(true);
  }

  public function status(Request $request)
  {
    $id = $request->id;
    $row = Product::whereId($id)->first();
    $row->status = $row->status == '1' ? '0' : '1';
    $row->save();
    return Helper::getStatus($row->status, $id, route('admin.product.status'));
  }


  public function delete(Request $request)
  {
    $user_id = $request->id;
    try {
      $delete = Product::where('id', '=', $user_id)->delete();
      if ($delete) {
        return ["type" => "success", "data" => "Record Deleted"];
      } else {
        return ["type" => "error", "data" => "Could not deleted Record"];
      }
    } catch (\Exception $e) {
      return ["type" => "error", "data" => $e->getMessage()];
    }
  }
  public function subcat(Request $request)
  {
    $id = $request->id;
    $getlist = SubCategory::where(['category_id' => $id, 'status' => '1'])->select('title', 'id')->get();
    if (!empty($getlist)) {
      foreach ($getlist as $key => $value) {
        $getlist[] = "<option value='" . $value->id . "'>" . $value->title . "</option>";
      }
      return ['status' => true, 'list' => $getlist, 'message' => 'Get successfully'];
    } else {
      $getlist = '<option value="">Sub Category not available</option>';
      return ['status' => false, 'list' => $getlist, 'message' => 'No record found'];
    }
  }
}
