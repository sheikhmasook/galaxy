<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Subscriber;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use DataTables;
use App\Lib\Helper;


class UsersController extends Controller
{
	public function index(){
       $page_title = 'Users List';
   	   $allusers = User::where(['status'=>'1','role'=>'user'])->get();
   	 return view('admin.users.index',compact('allusers','page_title'));
   }
   public function add(Request $request,$id=null){
       $page_title = 'Add User';
       $getuser = User::find($id);
        $breadcrumbs = [
      ['name' => 'Users', 'relation' => 'link', 'url' => route('admin.users.index')],
      ['name' => $page_title, 'relation' => 'Current', 'url' => ''],
    ];
       if($request->isMethod('post')){
        $postdata  = $request->all();
         $rules = [
          'name' => 'required',
          'phone_no' =>'required|string|min:10|max:10|unique:users,phone_no,'.$id,
          'gender' => 'nullable|in:male,female',
          'email' => 'nullable|email|unique:users,email,'.$id,
       ];
       $validator = Validator::make($postdata,$rules);
        if($id == null){
          $password = $this->randomPassword();
          $postdata['password'] = Hash::make($password); // creating password hash / encryption.
        }
        
          if ($validator->fails()) {
            return response()->json(array('errors' => $validator->messages()), 422);
          }else{ 
               $postdata['register_with'] = 'admin';
              if(!empty($request->hasFile('profile_picture'))){
                  $img = $request->file('profile_picture');
                  $file_ext = $img->getClientOriginalExtension();
                  $filename = time() . '.' . $file_ext;
                  $destination_path = 'uploads/users/'.$filename;
                    if(Storage::disk('public') -> put($destination_path, file_get_contents($img ->getRealPath()))){
                       $postdata['profile_picture'] = 'public/'.$destination_path;
                    }
               }
              if($id){
                 $getuser->update($postdata);
                  return ['status' => 'true', 'message' => 'Registration Update Successfully.'];
              }else{
                 User::create($postdata);
                 return ['status' => 'true', 'message' => 'Registration Create Successfully.'];
              }
              
          }
        
      }
       return view('admin.users.add',compact('getuser','page_title','id','breadcrumbs'));
   }
   public function dashboard(){
        $page_title = 'Dashboard';
       // total users
   	   $total_users = User::where(['role'=>'user'])->count();
       /* subscribers*/
       $tatal_subscriber = Subscriber::count();

       /*toral product*/
       $tatal_product = Product::count();
         /*toral Category*/
       $tatal_category = Category::count();
       
       // new users
       $new_users = User::where(['status'=>'1','role'=>'user','created_at'=>date('Y-m-d')])->count();
   	 return view('admin.users.dashboard',compact('total_users','page_title','new_users','tatal_subscriber','tatal_product','tatal_category'));
   }
   public function profile(Request $request){
       $page_title = 'Admin Profile';
       $id = Auth::guard('admin')->user()->id;
       $user = User::find($id);
       if($request->isMethod('post')){
         $postdata = $request->all();
         if($request->hasFile('profile_picture')){
            $img = $request->file('profile_picture');
            $file_ext = $img->getClientOriginalExtension();
            $filename = time() . '.' . $file_ext;
            $destination_path = 'uploads/users/'.$filename;
            if(Storage::disk('public') -> put($destination_path, file_get_contents($img ->getRealPath()))){
               $postdata['profile_picture'] = 'public/'.$destination_path;
            }
         }
         $user->update($postdata);
         return redirect()->route('admin.profile')->with('success','Profile updated Successfully');
       }
   	 return view('admin.users.profile',compact('page_title','user'));
   }
   public function datatables() {
    $users = User::where(['status'=>'1','role'=>'user'])
            ->select(['id', 'name', 'email', 'status', 'created_at', 'register_with'])
            ->get();

    return DataTables::of($users)
      ->addColumn('action', function ($user) {
        return '<a title="Edit" href="' . route('admin.users.add', $user->id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>&nbsp;
        <a title="View" href="' . route('admin.users.view', $user->id) . '" class="btn btn-xs btn-primary"><i class="fas fa-eye"></i></a>&nbsp;
        <a title="View" href="javascript:void(0)" class="btn btn-xs btn-danger" id="delete_' . $user->id . '" onclick="deleteuser(' . $user->id . ')")><i class="fas fa-trash" ></i></a>';
      })
      ->editColumn('type', function ($user) {
        return ucfirst($user->register_with);
      })
      ->editColumn('status', function ($user) {
        return Helper::getStatus($user->status, $user->id, route('admin.users.status'));
      })
      ->editColumn('created_at', function ($user) {
        return date("d M-Y", strtotime($user->created_at));
      })
      ->editColumn('name', function ($user) {
        return ucfirst($user->name);
      })
      ->rawColumns(['status', 'action', 'name', 'type'])
      ->make(true);
  }

  public function status(Request $request) {
    $id = $request->id;
    $row = User::whereId($id)->first();
    $row->status = $row->status == '1' ? '0' : '1';
    $row->save();
    return Helper::getStatus($row->status, $id, route('admin.users.status'));
  }

  public function view($id) {
    $data = User::where('id', $id)->first();
    if ($data) {
      $page_title = "Profile - " . $data->name;
      return view('admin.users.view', compact('page_title', 'data'));
    } else {
      return abort(404);
    }
  }
  public function delete(Request $request) {
    $user_id = $request->id;
    try {
      $delete = User::where('id', '=', $user_id)->delete();
      if ($delete) {
        return ["type" => "success", "data" => "Record Deleted"];
      } else {
        return ["type" => "error", "data" => "Could not deleted Record"];
      }
    } catch (\Exception $e) {
      return ["type" => "error", "data" => $e->getMessage()];
    }
  }
    public function changePassword(Request $request){
       $page_title = 'Change Password';
       $id = Auth::guard('admin')->user()->id;
       $user = User::find($id);
      if($request->isMethod('post')){
        $postdata  = $request->all();
        $validator = Validator::make($postdata,[
        'old_password' => 'required',
        'password' => 'required|min:8',
        'password_confirmation' => 'required|same:password|min:6',
       ]);
          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
          }else{
             if(Hash::check($postdata['old_password'], $user->password)) {
              $user->password = Hash::make($postdata['password']);
              $user->update($postdata);
              return redirect()->route('admin.dashboard')->with('success','Password Change Successfully');
             }
          }
        
      }
       return view('admin.users.change_password',compact('user','page_title'));
   }
}
