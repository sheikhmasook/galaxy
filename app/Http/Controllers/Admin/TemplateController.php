<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Template;
use DataTables;
use App\Lib\Helper;
use Illuminate\Support\Facades\Validator;

class TemplateController extends Controller
{
    public function index(){
      $page_title = 'Template List';
   	 return view('admin.template.index',compact('page_title'));

    }
    public function add(Request $request,$id=null){
       $page_title = 'Add Template';
       $getTemplate = Template::find($id);
       if($request->isMethod('post')){
        $postdata  = $request->all();
         $rules = [
          'title' => 'required',
          'subject' => 'required',
          'keywords' => 'required',
          'content' => 'required',
       ];
        $validator = Validator::make($postdata,$rules);
          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
          }else{ 
              if($id){
                 $getTemplate->update($postdata);
              return ['status' => 'true', 'message' => 'Template Update Successfully.'];
              }else{
                Template::create($postdata);
                return ['status' => 'true', 'message' => 'Template Create Successfully.'];
              }
              
          }
        
      }
       return view('admin.template.add',compact('getTemplate','page_title','id'));
   

    }

     public function datatables() {
      $templates = Template::where(['status'=>'1'])
            ->select(['id', 'title', 'subject', 'status', 'created_at', 'type'])
            ->get();

    return DataTables::of($templates)
      ->addColumn('action', function ($template) {
        return '<a title="Edit" href="' . route('admin.template.add', $template->id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>';
      })
      ->editColumn('type', function ($template) {
        return ($template->type == 1) ? "SMS" : "Email";
      })
      ->editColumn('status', function ($template) {
        return Helper::getStatus($template->status, $template->id, route('admin.template.status'));
      })
      
      ->editColumn('subject', function ($template) {
        return $template->subject;
      })
      ->editColumn('created_at', function ($template) {
        return date("d M-Y", strtotime($template->created_at));
      })
      ->editColumn('title', function ($template) {
        return ucfirst($template->title);
      })
      ->rawColumns(['status', 'action', 'title', 'type','subject','created_at'])
      ->make(true);
  }

  public function status(Request $request) {
    $id = $request->id;
    $row = Template::whereId($id)->first();
    $row->status = $row->status == '1' ? '2' : '1';
    $row->save();
    return Helper::getStatus($row->status, $id, route('admin.template.status'));
  }

}
