<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DataTables;
use App\Lib\Helper;
use Illuminate\Support\Facades\Storage;

class InvoiceController extends Controller
{
    public function index(){
       $page_title = 'Invoice List';
   	 return view('admin.Invoice.index',compact('page_title'));
   }
   public function add(Request $request,$id=null){
       $page_title = ($id) ? 'Edit Invoice':'Create Invoice';
       $getinvoice = Invoice::find($id);
       $breadcrumbs = [
			['name' => 'Invoice', 'relation' => 'link', 'url' => route('admin.invoice.index')],
			['name' => $page_title, 'relation' => 'Current', 'url' => ''],
		];
       if($request->isMethod('post')){
        $postdata  = $request->all();
         $rules = [
          'title' =>'required|string|min:2|max:100|unique:category,title,'.$id,
          'cat_id' =>'required',
          'sub_cat_id' =>'required',
          'colour' =>'required',
          'units' =>'required',
          'price' =>'required',
          'description' =>'nullable',
       ];

       $validator = Validator::make($postdata,$rules);
          if ($validator->fails()) {
          	return response()->json(array('errors' => $validator->messages()), 422);
          }else{ 

              if($id){
                 $getinvoice->update($postdata);
                  return ['status' => 'true', 'message' => 'Invoice Update Successfully.'];
              }else{
                 Product::create($postdata);
                 return ['status' => 'true', 'message' => 'Invoice Create Successfully.'];
              }
              
          }
        
      }
       $category_list = Category::pluck('title','id');
       $sub_category_list = '';
       $product_list = '';
       if($id){
         $sub_category_list = SubCategory::where(['id'=>$getinvoice->sub_cat_id])->pluck('title','id');
         $product_list = Product::where(['sub_cat_id'=>$getinvoice->sub_cat_id])->pluck('title','id');
        }
       return view('admin.Invoice.add',compact('getinvoice','page_title','id','breadcrumbs','category_list','sub_category_list','product_list'));
   }

   public function datatables() {
    $product = Invoice::where(['status'=>'1'])->with(['category','subcategory'])
            ->select(['id','cat_id','sub_cat_id','product_id', 'status', 'created_at'])
            ->get();
    return DataTables::of($product)
      ->addColumn('action', function ($product) {
        return '<a title="Edit" href="' . route('admin.product.add', $product->id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"> Edit</i></a>&nbsp;
        <a title="View" href="' . route('admin.product.delete', $product->id) . '" class="btn btn-xs btn-danger" onclick=return confirm("Are you sure you want to delete record?")><i class="fas fa-trash" > Delete</i></a>';
      })
      
      ->editColumn('status', function ($product) {
        return Helper::getStatus($product->status, $product->id, route('admin.product.status'));
      })
      ->editColumn('created_at', function ($product) {
        return date("d M-Y", strtotime($product->created_at));
      })
       ->editColumn('title', function ($product) {
        return $product->title;
      })
      ->editColumn('subcategory', function ($product) {
        return $product->subcategory->title;
      })
       ->editColumn('category', function ($product) {
        return $product->category->title;
      })
      ->rawColumns(['status', 'action', 'title','created_at','category','subcategory'])
      ->make(true);
  }

  public function status(Request $request) {
    $id = $request->id;
    $row = Product::whereId($id)->first();
    $row->status = $row->status == '1' ? '0' : '1';
    $row->save();
    return Helper::getStatus($row->status, $id, route('admin.invoice.status'));
  }

 
   public function delete($id) {
    $data = Product::find($id)->delete();
    if ($data) {
      return view('admin.Invoice.index', compact('page_title', 'data'));
    } else {
      return abort(404);
    }
  }
   public function subcat(Request $request) {
    $id = $request->id;
    $getlist = SubCategory::where(['category_id' => $id, 'status' => '1'])->select('title', 'id')->get();
    if (!empty($getlist)) {
      foreach ($getlist as $key => $value) {
        $getlist[] = "<option value='" . $value->id . "'>" . $value->title . "</option>";
      }
      return ['status' => true, 'list' => $getlist, 'message' => 'Get successfully'];
    } else {
      $getlist = '<option value="">Sub Category not available</option>';
      return ['status' => false, 'list' => $getlist, 'message' => 'No record found'];
    }

  }
  public function product(Request $request) {
    $subcat_id = $request->subcat_id;
    $getlist = Product::where(['sub_cat_id' => $subcat_id, 'status' => '1'])->select('title', 'id')->get();
    if (!empty($getlist)) {
    	//$getlist[] = "<option value=''>Select Product</option>";
      foreach ($getlist as $key => $value) {
        $getlist[] = "<option value='" . $value->id . "'>" . $value->title . "</option>";
      }
      return ['status' => true, 'list' => $getlist, 'message' => 'Get successfully'];
    } else {
      $getlist = '<option value="">Products not available</option>';
      return ['status' => false, 'list' => $getlist, 'message' => 'No record found'];
    }

  }
}

