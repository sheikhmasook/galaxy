<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lib\Helper;
use App\Models\Page;
use DataTables;
use Illuminate\Http\Request;
use Session;
use Validator;

class PageController extends Controller {
	public function __construct() {

	}

	public function add(Request $request, $id = null) {
		if ($id) {
			$title = "Edit Page";
			$breadcrumbs = [
				['name' => 'Pages', 'relation' => 'link', 'url' => route('admin.page.index')],
				['name' => 'Edit page', 'relation' => 'Current', 'url' => ''],
			];
		} else {
			$title = "Add New page";
			$breadcrumbs = [
				['name' => 'Pages', 'relation' => 'link', 'url' => route('admin.page.index')],
				['name' => 'Add New Page', 'relation' => 'Current', 'url' => ''],
			];
		}
		$data = ($id) ? Page::find($id) : array();
		if ($request->ajax() && $request->isMethod('post')) {
			try {
				$validator = Validator::make($request->all(), [
					'title' => 'required|max:255',
					'slug' => 'required|max:255|unique:pages,slug,' . $id,
					'description' => 'required',
				]);
				if ($validator->fails()) {
					return response()->json(array('errors' => $validator->messages()), 422);
				} else {
					$formData = $request->all();
					if ($id) {
						$data->update($formData);
						Session::flash('success', 'Page updated successfully');
					} else {
						Page::create($formData);
						Session::flash('success', 'Page created successfully');
					}
					return ['status' => 'true', 'message' => 'Records updated successfully'];
				}
			} catch (\Exception $e) {
				return ['status' => 'false', 'message' => $e->getMessage()];
			}
		}
		return view('admin/pages/add', compact('id', 'data', 'title', 'breadcrumbs'));
	}

	public function index() {
		$title = "Pages";
		$breadcrumbs = [
			['name' => 'Pages', 'relation' => 'Current', 'url' => ''],
		];
		return view('admin/pages/index', compact('title', 'breadcrumbs'));
	}

	public function datatables() {
		$pages = Page::select(['id', 'title', 'slug', 'status']);

		return DataTables::of($pages)
			->addColumn('action', function ($page) {
				return '<a href="' . route('admin.page.add', $page->id) . '" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a href="' . route('admin.page.view', $page->id) . '" class="btn btn-xs btn-primary"><i class="fas fa-eye"></i> View</a>';
			})
			->editColumn('status', function ($page) {
				return Helper::getStatus($page->status, $page->id, route('admin.page.status'));
			})
			->rawColumns(['status', 'action'])
			->make(true);
	}

	public function status(Request $request) {
		$id = $request->id;
		$row = Page::whereId($id)->first();
		$row->status = $row->status == '1' ? '0' : '1';
		$row->save();
		return Helper::getStatus($row->status, $id, route('admin.page.status'));
	}

	public function view($id) {
		$title = "Page Detail";
		$breadcrumbs = [
			['name' => 'Page', 'relation' => 'link', 'url' => route('admin.page.index')],
			['name' => $title, 'relation' => 'Current', 'url' => ''],
		];
		$data = Page::where('id', $id)->where('status', 1)->first();
		return view('admin.pages.view', compact('data', 'title', 'breadcrumbs'));

	}
}
