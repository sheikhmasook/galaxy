<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\SubCategory;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DataTables;
use App\Lib\Helper;

class SubCategoryController extends Controller
{
  public function index()
  {
    $page_title = 'Sub Category List';
    return view('admin.sub-category.index', compact('page_title'));
  }
  public function add(Request $request, $id = null)
  {
    $page_title = ($id) ? 'Edit SubCategory' : 'Add SubCategory';
    $getcategory = SubCategory::find($id);
    $breadcrumbs = [
      ['name' => 'SubCategory', 'relation' => 'link', 'url' => route('admin.sub-category.index')],
      ['name' => $page_title, 'relation' => 'Current', 'url' => ''],
    ];
    if ($request->isMethod('post')) {
      $postdata  = $request->all();
      $rules = [
        'category_id' => 'required',
        'title' => 'required|string|min:2|max:50|unique:sub_category,title,' . $id,
        'description' => 'nullable',
      ];

      $validator = Validator::make($postdata, $rules);
      if ($validator->fails()) {
        return response()->json(array('errors' => $validator->messages()), 422);
      } else {
        //return $postdata;
        if ($id) {
          $getcategory->update($postdata);
          return ['status' => 'true', 'message' => 'SubCategory Update Successfully.'];
        } else {
          SubCategory::create($postdata);
          return ['status' => 'true', 'message' => 'SubCategory Create Successfully.'];
        }
      }
    }
    $category_list = Category::pluck('title', 'id');
    return view('admin.sub-category.add', compact('getcategory', 'page_title', 'id', 'breadcrumbs', 'category_list'));
  }

  public function datatables()
  {
    $category = SubCategory::where(['status' => '1'])->with(['category'])
      ->select(['id', 'category_id', 'title', 'status', 'created_at'])
      ->get();
    return DataTables::of($category)
      ->addColumn('action', function ($category) {
        return '<a title="Edit" href="' . route('admin.sub-category.add', $category->id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>&nbsp;
        <a title="View" href="javascript:void(0)" class="btn btn-xs btn-danger" id="delete_' . $category->id . '" onclick="deleteuser(' . $category->id . ')")><i class="fas fa-trash" ></i></a>';
      })

      ->editColumn('status', function ($category) {
        return Helper::getStatus($category->status, $category->id, route('admin.sub-category.status'));
      })
      ->editColumn('created_at', function ($category) {
        return date("d M-Y", strtotime($category->created_at));
      })
      ->editColumn('cat_title', function ($category) {
        return ($category->category) ? $category->category->title : "N/A";
      })
      ->editColumn('title', function ($category) {
        return $category->title;
      })
      ->rawColumns(['status', 'action', 'title', 'created_at'])
      ->make(true);
  }

  public function status(Request $request)
  {
    $id = $request->id;
    $row = SubCategory::whereId($id)->first();
    $row->status = $row->status == '1' ? '0' : '1';
    $row->save();
    return Helper::getStatus($row->status, $id, route('admin.sub-category.status'));
  }


  public function delete(Request $request)
  {
    $user_id = $request->id;
    try {
      $delete = SubCategory::where('id', '=', $user_id)->delete();
      if ($delete) {
        return ["type" => "success", "data" => "Record Deleted"];
      } else {
        return ["type" => "error", "data" => "Could not deleted Record"];
      }
    } catch (\Exception $e) {
      return ["type" => "error", "data" => $e->getMessage()];
    }
  }
}
