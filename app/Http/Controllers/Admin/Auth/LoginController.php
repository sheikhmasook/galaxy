<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin\dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest')->except('logout');
    }
    public function login(Request $request){
       
        $title_page = 'Login';
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required|min:6',
            ]);
            if ($validator->fails()) {
                return redirect()->route('admin.login')->withInput()->withErrors($validator->errors());
            } else {
                $email = $request->email;
                $password = $request->password;
                $credentials = ['email' => $email, 'password' => $password,'role'=>'admin','status' => 1];
                if (Auth::guard('admin')->attempt($credentials)) {
                    return redirect()->intended('admin/dashboard')->with('success', 'Login Successfully');
                } else {
                    return redirect()->back()->withInput()->with('danger', 'Invalid email or password or activate your email.');
                }
            }
        }    
        return view('admin.auth.login');

    }
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login')->with('success','Logout Successfully!');
    }


}
