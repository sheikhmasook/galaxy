<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Unit;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DataTables;
use App\Lib\Helper;

class UnitController extends Controller
{
    public function index()
    {
        $page_title = 'Unit List';
        return view('admin.unit.index', compact('page_title'));
    }
    public function add(Request $request, $id = null)
    {
        $page_title = ($id) ? 'Edit unit' : 'Add unit';
        $getunit = Unit::find($id);
        $breadcrumbs = [
            ['name' => 'Unit', 'relation' => 'link', 'url' => route('admin.unit.index')],
            ['name' => $page_title, 'relation' => 'Current', 'url' => ''],
        ];
        if ($request->isMethod('post')) {
            $postdata  = $request->all();
            $rules = [
                'title' => 'required|string|min:2|max:10|unique:units,title,' . $id,
            ];

            $validator = Validator::make($postdata, $rules);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                //return $postdata;
                if ($id) {
                    $getcategory->update($postdata);
                    return ['status' => 'true', 'message' => 'Unit Update Successfully.'];
                } else {
                    Unit::create($postdata);
                    return ['status' => 'true', 'message' => 'Unit Create Successfully.'];
                }
            }
        }
        return view('admin.unit.add', compact('getunit', 'page_title', 'id', 'breadcrumbs'));
    }

    public function datatables()
    {
        $unit = Unit::where(['status' => '1'])
            ->select(['id', 'title', 'status', 'created_at'])
            ->get();

        return DataTables::of($unit)
            ->addColumn('action', function ($unit) {
                return '<a title="Edit" href="' . route('admin.unit.add', $unit->id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>&nbsp;
         <a title="View" href="javascript:void(0)" class="btn btn-xs btn-danger" id="delete_' . $unit->id . '" onclick="deleteuser(' . $unit->id . ')")><i class="fas fa-trash" ></i></a>';
            })

            ->editColumn('status', function ($unit) {
                return Helper::getStatus($unit->status, $unit->id, route('admin.unit.status'));
            })
            ->editColumn('created_at', function ($unit) {
                return date("d M-Y", strtotime($unit->created_at));
            })
            ->editColumn('title', function ($unit) {
                return $unit->title;
            })
            ->rawColumns(['status', 'action', 'title', 'created_at'])
            ->make(true);
    }

    public function status(Request $request)
    {
        $id = $request->id;
        $row = Unit::whereId($id)->first();
        $row->status = $row->status == '1' ? '0' : '1';
        $row->save();
        return Helper::getStatus($row->status, $id, route('admin.unit.status'));
    }


    public function delete(Request $request)
    {
        $user_id = $request->id;
        try {
            $delete = Unit::where('id', '=', $user_id)->delete();
            if ($delete) {
                return ["type" => "success", "data" => "Record Deleted"];
            } else {
                return ["type" => "error", "data" => "Could not deleted Record"];
            }
        } catch (\Exception $e) {
            return ["type" => "error", "data" => $e->getMessage()];
        }
    }
}
