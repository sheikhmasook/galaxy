<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lib\Helper;
use App\Models\Faq;
use DataTables;
use Illuminate\Http\Request;
use Session;
use Validator;

class FaqController extends Controller {
	public function __construct() {

	}

	public function add(Request $request, $id = null) {
		if ($id) {
			$title = "Edit Faq";
			$breadcrumbs = [
				['name' => 'Pages', 'relation' => 'link', 'url' => route('admin.faq.index')],
				['name' => 'Edit faq', 'relation' => 'Current', 'url' => ''],
			];
		} else {
			$title = "Add New faq";
			$breadcrumbs = [
				['name' => 'Faq', 'relation' => 'link', 'url' => route('admin.faq.index')],
				['name' => 'Add new faq', 'relation' => 'Current', 'url' => ''],
			];
		}
		$data = ($id) ? Faq::find($id) : array();
		if ($request->ajax() && $request->isMethod('post')) {
			try {
				$validator = Validator::make($request->all(), [
					'title' => 'required|max:255',
					'description' => 'required',
				]);
				if ($validator->fails()) {
					return response()->json(array('errors' => $validator->messages()), 422);
				} else {
					$formData = $request->all();
					if ($id) {
						$data->update($formData);
						Session::flash('success', 'Faq updated successfully');
					} else {
						Faq::create($formData);
						Session::flash('success', 'Faq created successfully');
					}
					return ['status' => 'true', 'message' => 'Records updated successfully'];
				}
			} catch (\Exception $e) {
				return ['status' => 'false', 'message' => $e->getMessage()];
			}
		}
		return view('admin/faq/add', compact('id', 'data', 'title', 'breadcrumbs'));
	}

	public function index() {
		$title = "FAQ";
		$breadcrumbs = [
			['name' => 'FAQ', 'relation' => 'Current', 'url' => ''],
		];
		return view('admin.faq.index', compact('title', 'breadcrumbs'));
	}

	public function datatables() {
		$pages = Faq::select(['id', 'title', 'status']);
		return DataTables::of($pages)
			->addColumn('action', function ($page) {
				return '<a href="' . route('admin.faq.add', $page->id) . '" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a href="' . route('admin.faq.view', $page->id) . '" class="btn btn-xs btn-primary"><i class="fas fa-eye"></i> View</a>';
			})
			->editColumn('status', function ($page) {
				return Helper::getStatus($page->status, $page->id, route('admin.faq.status'));
			})
			->rawColumns(['status', 'action'])
			->make(true);
	}

	public function status(Request $request) {
		$id = $request->id;
		$row = faq::whereId($id)->first();
		$row->status = $row->status == '1' ? '0' : '1';
		$row->save();
		return Helper::getStatus($row->status, $id, route('admin.faq.status'));
	}

	public function view($id) {
		$title = "FAQ Detail";
		$breadcrumbs = [
			['name' => 'Faq', 'relation' => 'link', 'url' => route('admin.faq.index')],
			['name' => $title, 'relation' => 'Current', 'url' => ''],
		];
		$data = Faq::where('id', $id)->where('status', 1)->first();
		return view('admin.faq.view', compact('data', 'title', 'breadcrumbs'));

	}
}
