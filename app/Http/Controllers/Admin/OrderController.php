<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\Order;
use App\Models\User;
use App\Models\OrderItem;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DataTables;
use App\Lib\Helper;
use Illuminate\Support\Facades\Storage;
use PDF;

class OrderController extends Controller
{
  public function index()
  {
    $page_title = 'Order List';
    return view('admin.Order.index', compact('page_title'));
  }
  public function add(Request $request, $id = null)
  {
    $page_title = ($id) ? 'Edit Order' : 'Create Order';
    $getProduct = Order::find($id);
    $breadcrumbs = [
      ['name' => 'Order', 'relation' => 'link', 'url' => route('admin.order.index')],
      ['name' => $page_title, 'relation' => 'Current', 'url' => ''],
    ];
    if ($request->isMethod('post')) {
      $postdata  = $request->all();
      $rules = [
        'product_id.*' => 'required',
        'cat_id.*' => 'required',
        'sub_cat_id.*' => 'required',
        'units.*' => 'required',
        'qty.*' => 'required',
        'price.*' => 'required',
        'weight.*' => 'required',
        'user_id' => 'required',
      ];

      $validator = Validator::make($postdata, $rules);
      if ($validator->fails()) {
        return response()->json(array('errors' => $validator->messages()), 422);
      } else {

        $order = new Order();
        $order->user_id = $postdata['user_id'];
        $order->discount = 0.00;
        $order->sub_total = 0.00;
        $order->tax =  0.00;
        $order->total = 0.00;
        $order->save();
        $sub_total = $order_items_data = $order_data = [];
        foreach ($postdata['cat_id'] as $key => $value) {
          $order_items_data['cat_id']  = $postdata['cat_id'][$key];
          $order_items_data['sub_cat_id']  = $postdata['sub_cat_id'][$key];
          $order_items_data['product_id']  = $postdata['product_id'][$key];
          $order_items_data['weight']  = $postdata['weight'][$key];
          $order_items_data['qty']  = $postdata['qty'][$key];
          $order_items_data['unit']  = $postdata['units'][$key];
          $order_items_data['price']  = $postdata['price'][$key];
          $sub_total[] = ($postdata['price'][$key] * $postdata['qty'][$key]);
          $order_items_data['order_id'] = $order['id'];
        }
        $sub_total = array_sum($sub_total);
        $order->sub_total = $sub_total;
        $order->total = $sub_total;
        $order->save();
        if ($id) {
          $getProduct->update($getProduct);
          return ['status' => 'true', 'message' => 'Order has been Update Successfully.'];
        } else {
          OrderItem::create($order_items_data);
          return ['status' => 'true', 'message' => 'Order has been created Successfully.'];
        }
      }
    }
    $category_list = Category::pluck('title', 'id');
    $user_list = User::where('role', 'user')->pluck('name', 'id');
    $sub_category_list = '';
    $product_list = '';
    if ($id) {
      $sub_category_list = SubCategory::where(['id' => $getProduct->sub_cat_id])->pluck('title', 'id');
      $product_list = Product::where(['sub_cat_id' => $getProduct->sub_cat_id])->pluck('title', 'id');
    }
    return view('admin.Order.add', compact('getProduct', 'page_title', 'id', 'breadcrumbs', 'category_list', 'sub_category_list', 'product_list', 'user_list'));
  }

  public function datatables()
  {
    $orders = Order::with(['orderItem.product', 'user'])->get();
    return DataTables::of($orders)
      ->addColumn('action', function ($products) {
        return '<a title="Invoice" href="' . route('admin.order.invoice', $products->id) . '" class="btn btn-xs btn-info"><i class="fas fa-file-invoice"></i></a>&nbsp;
        <a title="order detail" href="' . route('admin.order.view', $products->id) . '" class="btn btn-xs btn-info"><i class="fas fa-eye"></i></a>&nbsp;
        <a title="download-invoice" href="' . route('admin.invoice.download', $products->id) . '" class="btn btn-xs btn-info"><i class="fas fa-file-download"></i></a>';
      })

      ->editColumn('created_at', function ($products) {
        return date("d M-Y", strtotime($products->created_at));
      })
      //  ->editColumn('title', function ($products) {
      //   return ($products->order_item->product) ? $products->order_item->product->title : "N/A";
      // })
      ->editColumn('user.name', function ($products) {
        return ($products->user) ? $products->user->name : "N/A";
      })
      ->editColumn('total', function ($products) {
        return ($products->total) ? $products->total : "N/A";
      })
      ->rawColumns(['action', 'name', 'created_at', 'title', 'total'])
      ->make(true);
  }

  public function status(Request $request)
  {
    $id = $request->id;
    $row = Product::whereId($id)->first();
    $row->status = $row->status == '1' ? '0' : '1';
    $row->save();
    return Helper::getStatus($row->status, $id, route('admin.invoice.status'));
  }


  public function delete($id)
  {
    $data = Product::find($id)->delete();
    if ($data) {
      return view('admin.Invoice.index', compact('page_title', 'data'));
    } else {
      return abort(404);
    }
  }
  public function subcat(Request $request)
  {
    $id = $request->id;
    $getlist = SubCategory::where(['category_id' => $id, 'status' => '1'])->select('title', 'id')->get();
    if (!empty($getlist)) {
      foreach ($getlist as $key => $value) {
        $getlist[] = '<option value="">Select Sub Category</option>';
        $getlist[] = "<option value='" . $value->id . "'>" . $value->title . "</option>";
      }
      return ['status' => true, 'list' => $getlist, 'message' => 'Get successfully'];
    } else {
      $getlist = '<option value="">Sub Category not available</option>';
      return ['status' => false, 'list' => $getlist, 'message' => 'No record found'];
    }
  }

  public function product(Request $request)
  {
    $subcat_id = $request->subcat_id;
    $getlist = Product::where(['sub_cat_id' => $subcat_id, 'status' => '1'])->select('title', 'id', 'weight')->get();
    if (!empty($getlist)) {
      foreach ($getlist as $key => $value) {
        $getlist[] = "<option value='" . $value->id . "'>" . $value->title . "</option>";
        $weight_list[] = "<option value='" . $value->weight . "'>" . $value->weight . "</option>";
      }
      return ['status' => true, 'list' => $getlist, 'weight_list' => $weight_list, 'message' => 'Get successfully'];
    } else {
      $getlist = '<option value="">Products not available</option>';
      return ['status' => false, 'list' => $getlist, 'message' => 'No record found'];
    }
  }
  public function productWeight(Request $request)
  {
    $product_id = $request->product_id;
    $getlist = Product::where(['id' => $product_id, 'status' => '1'])->select('weight', 'id')->get();
    if (!empty($getlist)) {
      foreach ($getlist as $key => $value) {
        $getlist[] = "<option value='" . $value->id . "'>" . $value->weight . "</option>";
      }
      return ['status' => true, 'list' => $getlist, 'message' => 'Get successfully'];
    } else {
      $getlist = '<option value="">Weight not available</option>';
      return ['status' => false, 'list' => $getlist, 'message' => 'No record found'];
    }
  }

  public function invoice($id)
  {
    $order = Order::with(['orderItem.product', 'user'])->where('id', $id)->first();
    // echo '<pre>';
    // print_r($order->toArray());die;
    return view('admin.Order.invoice', compact('order'));
  }
  public function downloadfile($id)
  {
    $order = Order::with(['orderItem.product', 'user'])->where('id', $id)->first();
    $base_url = url('/');
    $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'adminUsername' => true, 'adminPassword' => true])->loadView('admin.Order.pdf-invoice', compact('order', 'base_url'));
    return $pdf->download('Galaxy_Marble_invoice.pdf');
  }
  public function printfile($id)
  {
    $order = Order::with(['orderItem.product', 'user'])->where('id', $id)->first();
    $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'adminUsername' => true, 'adminPassword' => true])->loadView('admin.Order.pdf-invoice', compact('order'));
    return $pdf->stream();
  }
  public function view($id)
  {
    $page_title = 'Order detail';
    $breadcrumbs = [
      ['name' => 'Order', 'relation' => 'link', 'url' => route('admin.order.index')],
      ['name' => $page_title, 'relation' => 'Current', 'url' => ''],
    ];
    $order = Order::with(['orderItem.product', 'user'])->where('id', $id)->first();

    return view('admin.Order.view', compact('order', 'page_title'));
  }

  public function productPrice(Request $request)
  {
    $cat_id = $request->cat_id;
    $subcat_id = $request->subcat_id;
    $product_id = $request->product_id;
    $weight = $request->weight;
    $unit_id = $request->unit_id;
    $condition = [
      ['id', '=', $product_id],
      // ['weight', '=', $weight],
      ['units', '=', $unit_id],
      ['status', '=', 1],
    ];
    // return $condition;
    $getProduct = Product::where($condition)->first();
    //  return $getProduct;
    if (!empty($getProduct)) {
      return ['status' => true, 'price' => $getProduct->price, 'message' => 'Price get successfully'];
    } else {
      return ['status' => false, 'message' => 'No record found'];
    }
  }
}
