<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DataTables;
use App\Lib\Helper;
use App\Models\Subscriber;

class SubscriberController extends Controller
{
    public function index(){
      $page_title = 'Subscriber List';
   	 return view('admin.subscriber.index',compact('page_title'));
   }

   public function datatables() {
    $subscriber = Subscriber::all();
    return DataTables::of($subscriber)
      ->addColumn('action', function ($subscriber) {
        return '<a title="View" href="javascript:void(0)" class="btn btn-xs btn-danger" id="delete_' . $subscriber->id . '" onclick="deleteuser(' . $subscriber->id . ')")><i class="fas fa-trash" ></i></a>';
      })
      ->editColumn('created_at', function ($subscriber) {
        return date("d-M-Y", strtotime($subscriber->created_at));
      })
      
      ->rawColumns(['action','created_at'])
      ->make(true);
  }
  public function delete(Request $request) {
    $user_id = $request->id;
    try {
      $delete = Subscriber::where('id', '=', $user_id)->delete();
      if ($delete) {
        return ["type" => "success", "data" => "Record Deleted"];
      } else {
        return ["type" => "error", "data" => "Could not deleted Record"];
      }
    } catch (\Exception $e) {
      return ["type" => "error", "data" => $e->getMessage()];
    }
  }
 
  
 

  
}

