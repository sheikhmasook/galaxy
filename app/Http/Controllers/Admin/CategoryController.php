<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DataTables;
use App\Lib\Helper;

class CategoryController extends Controller
{
    public function index(){
       $page_title = 'Category List';
   	   $allcategory = Category::where(['status'=>'1'])->get();
   	 return view('admin.category.index',compact('allcategory','page_title'));
   }
   public function add(Request $request,$id=null){
       $page_title = ($id) ? 'Edit category':'Add Category';
       $getcategory = Category::find($id);
       $breadcrumbs = [
			['name' => 'Category', 'relation' => 'link', 'url' => route('admin.category.index')],
			['name' => $page_title, 'relation' => 'Current', 'url' => ''],
		];
       if($request->isMethod('post')){
        $postdata  = $request->all();
         $rules = [
          'title' =>'required|string|min:2|max:10|unique:category,title,'.$id,
          'description' =>'nullable',
       ];

       $validator = Validator::make($postdata,$rules);
          if ($validator->fails()) {
          	return response()->json(array('errors' => $validator->messages()), 422);
          }else{ 
          	//return $postdata;
              if($id){
                 $getcategory->update($postdata);
                  return ['status' => 'true', 'message' => 'Category Update Successfully.'];
              }else{
                 Category::create($postdata);
                 return ['status' => 'true', 'message' => 'Category Create Successfully.'];
              }
              
          }
        
      }
       return view('admin.category.add',compact('getcategory','page_title','id','breadcrumbs'));
   }

   public function datatables() {
    $category = Category::where(['status'=>'1'])
            ->select(['id', 'title', 'status', 'created_at'])
            ->get();

    return DataTables::of($category)
      ->addColumn('action', function ($category) {
        return '<a title="Edit" href="' . route('admin.category.add', $category->id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>&nbsp;
         <a title="View" href="javascript:void(0)" class="btn btn-xs btn-danger" id="delete_' . $category->id . '" onclick="deleteuser(' . $category->id . ')")><i class="fas fa-trash" ></i></a>';
      })
      
      ->editColumn('status', function ($category) {
        return Helper::getStatus($category->status, $category->id, route('admin.category.status'));
      })
      ->editColumn('created_at', function ($category) {
        return date("d M-Y", strtotime($category->created_at));
      })
      ->editColumn('title', function ($category) {
        return $category->title;
      })
      ->rawColumns(['status', 'action', 'title','created_at'])
      ->make(true);
  }

  public function status(Request $request) {
    $id = $request->id;
    $row = Category::whereId($id)->first();
    $row->status = $row->status == '1' ? '0' : '1';
    $row->save();
    return Helper::getStatus($row->status, $id, route('admin.category.status'));
  }

 
    public function delete(Request $request) {
    $user_id = $request->id;
    try {
      $delete = Category::where('id', '=', $user_id)->delete();
      if ($delete) {
        return ["type" => "success", "data" => "Record Deleted"];
      } else {
        return ["type" => "error", "data" => "Could not deleted Record"];
      }
    } catch (\Exception $e) {
      return ["type" => "error", "data" => $e->getMessage()];
    }
  }
}
