<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Validator;

class UserController extends Controller
{
	// This method use for checkUser
	protected function checkUser(Request $request)
	{
		DB::beginTransaction();
		try {
			$data = $request->all();
			$validator = Validator::make($data, [
				'social_id' => 'required',
				'device_type' => 'required|in:IOS,ANDROID',
				'device_token' => 'required',
				'social_type' => 'required|in:FACEBOOK,GOOGLE',
				'name' => 'required',
				'email' => 'nullable|email',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => 'false', 'message' => $error]);
			} else {
				if (isset($data['email']) && !empty($data['email'])) {
					$user = User::where('email', '=', $data['email'])->first();
				} else {
					$user = User::where('social_id', '=', $data['social_id'])->first();
				}
				if ($user) {
					UserDevices::deviceHandle([
						"id" => $user->id,
						"device_type" => $data['device_type'],
						"device_token" => $data['device_token'],
					]);
					$jwtResponse = User::authorizeUserToken($user);
					DB::commit();
					return response()->json(['status' => 'true', 'message' => 'User Details', 'data' => $user, 'security_token' => @$jwtResponse['token']]);
				} else {
					$formData = [
						'social_id' => $data['social_id'],
						'social_type' => $data['social_type'],
						'user_type' => "Social",
						'status' => 1,
					];
					if ($request->has('email') && $request->get('email') != '') {
						$formData['email'] = $request->get('email');
					}
					if ($request->has('name') && $request->get('name') != '') {
						$formData['name'] = $request->get('name');
					}
					if ($request->has('mobile') && $request->get('mobile') != '') {
						$formData['mobile'] = $request->get('mobile');
					}
					$user = User::create($formData);
					$user = User::find($user->id);
					if ($user) {
						UserDevices::deviceHandle([
							"id" => $user->id,
							"device_type" => $data['device_type'],
							"device_token" => $data['device_token'],
						]);
					}
					$jwtResponse = User::authorizeUserToken($user);
					DB::commit();
					return response()->json(['status' => 'true', 'message' => 'User Details', 'data' => $user, 'security_token' => @$jwtResponse['token']]);
				}
			}
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'false', 'message' => $e->getMessage(), 'data' => []]);
		}
	}
	// This method use for signup
	protected function signup(Request $request)
	{
		DB::beginTransaction();
		try {
			$data = $request->all();
			$validator = Validator::make($data, [
				'name' => 'required|max:45',
				'email' => 'required|unique:users|email',
				'password' => 'required|min:8|max:45',
				'device_type' => 'required|in:IOS,ANDROID',
				'device_token' => 'required',
				'dob' => 'required|date_format:Y-m-d',
				'city' => 'required|max:45',
				//'country_id'        => 'required|numeric|exists:countries,id',
				'country_name' => 'required|max:70',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => 'false', 'message' => $error]);
			} else {
				$token = $this->random(45);
				$formData = $request->except('password');
				$formData['password'] = Hash::make($request->get('password'));
				$formData['token'] = $token;
				$formData['status'] = 2;
				$formData['user_type'] = 'App';
				$user = User::create($formData);
				$user = User::find($user->id);
				// Send Email
				$email_data['name'] = $user->name;
				$email_data['link'] = '<a class="btn-mail" href="' . route('users.verify', $token) . '">Click to verify your Email</a>';
				$email_data['url'] = route('users.verify', $token);
				Email::send('email-verification', $email_data, $user->email);
				if ($user) {
					UserDevices::deviceHandle([
						"id" => $user->id,
						"device_type" => $data['device_type'],
						"device_token" => $data['device_token'],
					]);
				}
				$message = "User has been registered successfully, Please verify your email address.";
				DB::commit();
				return response()->json(['status' => 'true', 'message' => $message, 'data' => []]);
			}
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'false', 'message' => $e->getMessage(), 'data' => []]);
		}
	}

	// This method use for signin
	protected function signin(Request $request)
	{
		DB::beginTransaction();
		try {
			$data = $request->all();

			$validator = Validator::make($data, [
				'email' => 'required|email',
				'password' => 'required',
				'device_type' => 'required|in:IOS,ANDROID',
				'device_token' => 'required',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => 'false', 'message' => $error]);
			} else {
				$user = User::where('email', $data['email'])->first();
				if (!$user) {
					return response()->json(['status' => 'false', 'message' => 'Email not exist.', 'data' => []]);
				} else {
					if (Hash::check($data['password'], $user->password)) {
						if ($user->status == 2) {
							return response()->json(['status' => 'verification', 'message' => 'Your email is not verified, please verify your email address.', 'data' => []]);
						} else if ($user->status == 0) {
							return response()->json(['status' => 'false', 'message' => 'Your account is inactive, please contact to administrator.', 'data' => []]);
						} else if ($user->status == 3) {
							return response()->json(['status' => 'false', 'message' => 'Your account is deleted, please contact to administrator.', 'data' => []]);
						} else {
							$jwtResponse = User::authorizeToken($user);
							UserDevices::deviceHandle([
								"id" => $user->id,
								"device_type" => $data['device_type'],
								"device_token" => $data['device_token'],
							]);
							$security_token = @$jwtResponse['token'];
							DB::commit();
							return response()->json(['status' => 'true', 'message' => 'Signin sucessfully.', 'data' => $user, 'security_token' => $security_token]);
						}
					} else {
						DB::commit();
						return response()->json(['status' => 'false', 'message' => 'Incorrect Password.', 'data' => []]);
					}
				}
			}
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'false', 'message' => $e->getMessage(), 'data' => []]);
		}
	}

	// This method use for get profile
	public function getProfile(Request $request)
	{
		DB::beginTransaction();
		try {
			$userId = JWTAuth::toUser(JWTAuth::getToken())->id;
			$profile = User::getProfile($userId);
			if (count($profile) > 0) {
				DB::commit();
				return response()->json(['status' => 'true', 'message' => 'User Profile', 'data' => $profile]);
			} else {
				DB::commit();
				return response()->json(['status' => 'false', 'message' => 'User Not Found', 'data' => []]);
			}
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'false', 'message' => $e->getMessage(), 'data' => []]);
		}
	}

	// This method use for update profile
	public function updateProfile(Request $request)
	{
		DB::beginTransaction();
		try {
			$data = $request->all();
			$userId = JWTAuth::toUser(JWTAuth::getToken())->id;
			$validator = Validator::make($request->all(), [
				'name' => 'required|max:45',
				'profile_picture' => 'nullable|image',
				'dob' => 'required|date_format:Y-m-d',
				'city' => 'required|max:70',
				//'country_id'        => 'required|numeric|exists:countries,id',
				'country_name' => 'required|max:70',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => 'false', 'message' => $error]);
			} else {
				$user = User::find($userId);
				$user->name = $data['name'];
				$user->city = $data['city'];
				$user->dob = $data['dob'];
				//$user->country_id  = $data['country_id'];
				$user->country_name = $data['country_name'];
				if ($request->file('profile_picture') !== null) {
					$destinationPath = '/uploads/profile/';
					$responseData = Uploader::doUpload($request->file('profile_picture'), $destinationPath, true);
					if ($responseData['status'] == "true") {
						$user->profile_picture = $responseData['file'];
					}
				}
				if ($user->save()) {
					$data = User::getProfile($user->id);
					DB::commit();
					return response()->json(['status' => 'true', 'message' => 'Profile updated successfully.', 'data' => $data]);
				} else {
					$data = User::getProfile($user->id);
					DB::commit();
					return response()->json(['status' => 'false', 'message' => 'Unknown error accured while updating information.', 'data' => $data]);
				}
			}
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'false', 'message' => $e->getMessage(), 'data' => []]);
		}
	}

	// This method use for change password
	public function changePassword(Request $request)
	{
		DB::beginTransaction();
		try {
			$data = $request->all();
			$validator = Validator::make($request->all(), [
				'current_password' => 'required|min:8|max:45',
				'new_password' => 'required|min:8|max:45',
				'confirm_password' => 'required|same:new_password',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => 'false', 'message' => $error]);
			} else {
				$userId = JWTAuth::toUser(JWTAuth::getToken())->id;
				$user = User::find($userId);
				if (!$user) {
					DB::commit();
					return response()->json(['status' => 'false', 'message' => 'User Not Found.', 'data' => []]);
				} else {
					if (Hash::check($data['current_password'], $user->password)) {
						$user->password = bcrypt($data['new_password']);
						$user->save();
						DB::commit();
						return response()->json(['status' => 'true', 'message' => 'Password change successfully.', 'data' => []]);
					} else {
						DB::commit();
						return response()->json(['status' => 'false', 'message' => "Current Password doesn't match.", 'data' => []]);
					}
				}
			}
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'false', 'message' => $e->getMessage(), 'data' => []]);
		}
	}

	// This method use for forgot password
	public function forgot(Request $request)
	{
		DB::beginTransaction();
		try {
			$data = $request->all();
			$validator = Validator::make($data, ['email' => 'required|email']);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => 'false', 'message' => $error]);
			} else {
				$user = User::where('email', $data['email'])->first();
				if (!$user) {
					DB::commit();
					return response()->json(['status' => 'false', 'message' => 'Email does not exist.']);
				} else {
					if ($user->status == 2) {
						DB::commit();
						return response()->json(['status' => 'false', 'message' => 'Your email is not verified.', 'data' => []]);
					} else if ($user->status == 0) {
						DB::commit();
						return response()->json(['status' => 'false', 'message' => 'Your account is inactive, please contact to administrator.', 'data' => []]);
					} else if ($user->status == 3) {
						DB::commit();
						return response()->json(['status' => 'false', 'message' => 'Your account is deleted, please contact to administrator.', 'data' => []]);
					} else {
						$token = $this->random(45);
						$user->token = $token;
						$user->save();
						$email_data['link'] = '<a class="btn-mail" href="' . route('users.reset_password', $token) . '">New Reset Password</a>';
						$email_data['url'] = route('users.reset_password', $token);
						Email::send('reset-password', $email_data, $data['email']);
						DB::commit();
						return response()->json(['status' => 'true', 'message' => 'Reset password link has been sent successfully to your email address.']);
					}
				}
			}
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'false', 'message' => $e->getMessage(), 'data' => []]);
		}
	}

	// This method use for logout user.
	public function logout(Request $request)
	{
		try {
			$validator = Validator::make($request->all(), [
				'device_type' => 'required|in:ANDROID,IOS',
				'device_token' => 'required',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				DB::commit();
				return response()->json(['status' => 'false', 'message' => $error]);
			} else {
				$userDevice = UserDevices::where(['device_type' => $request['device_type'], 'device_token' => $request['device_token']])->first();
				$token = JWTAuth::getToken();
				if ($token) {
					JWTAuth::setToken($token)->invalidate();
				}
				if ($userDevice) {
					UserDevices::where(['device_type' => $request['device_type'], 'device_token' => $request['device_token']])->delete();
				}
				return response()->json(['status' => 'true', 'message' => 'Logout Successfully.']);
			}
		} catch (Exception $e) {
			return response()->json(['status' => 'false', 'message' => $e->getMessage(), 'data' => []]);
		}
	}
}
