<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Lib\Email;
use App\Models\Template;
use App\Models\Subscriber;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('Front.Home.index');
    }
    public function subscribe(Request $request){
        if($request->isMethod('post')){
            $postdata  = $request->all();
             $rules = [
               'email' => 'required|email|unique:subscribers,email',
           ];
            $validator = Validator::make($postdata,$rules);
          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
          }else{ 
                 Subscriber::create($postdata);
                 $emailTemplate = Template::where('id','=','10')->first();
                 $content = $emailTemplate->content;
                 $subject = $emailTemplate->subject;
                    $site_url = url('/');
                    $logo_img = url('/public/Admin/img/logo.png');
                    $support_link = '#';
                    $terms_condition = '#';
                    $download_android_link = '#';
                    $download_iOS_link = '#';
                    $content = str_replace(['[site_url]', '[logo_img]', '[support_link]', '[terms_condition]', '[download_android_link]', '[download_iOS_link]',
                    ], [$site_url, $logo_img, $support_link, $terms_condition, $download_android_link, $download_iOS_link], $content);
                  Email::sendEmail($content,$postdata['email'],$subject);
                  return redirect()->route('front.home.index')->with('success','Thanks for subscribing with us');              
           }
          
          }   
        } 
    
}