<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model{
    use HasFactory;
     protected $fillable = ['order_id','cat_id','sub_cat_id','product_id','weight','unit','qty','price'
         ];
        public function category() {
			  return $this->belongsTo('App\Models\Category', 'cat_id');
			}
		public function subcategory() {
		  return $this->belongsTo('App\Models\SubCategory', 'sub_cat_id');
		}
		public function product() {
		  return $this->belongsTo('App\Models\Product', 'product_id');
		}
		
		
}
