<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use JWTAuth;
class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
        protected $fillable = ['name','email','phone_no','password','profile_picture','gender','register_with',
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }
    public static function authorizeToken($request) {
        $credentials = $request;
        try {
            if (!$token = JWTAuth::fromUser($credentials)) {
                return ['status' => 'false', 'error' => 'invalid_credentials', 'code' => '401'];
            }
        } catch (JWTException $e) {
            return ['status' => 'false', 'error' => 'could_not_create_token', 'code' => 500];
        }
        return ['status' => 'true', 'token' => $token, 'code' => 200];
    }

    public static function authorizeUserToken($request) {
        $token = JWTAuth::fromUser($request);
        try {
            if (!$token) {
                return ['status' => 'false', 'error' => 'invalid_credentials', 'code' => '401'];
            }
        } catch (JWTException $e) {
            return ['status' => 'false', 'error' => 'could_not_create_token', 'code' => 500];
        }
        return ['status' => 'true', 'token' => $token, 'code' => 200];
    }    
}
