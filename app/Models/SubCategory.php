<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
	
    use HasFactory;
	  protected $table = 'sub_category';
      protected $fillable = ['category_id','title','description',
        ];
			public function category() {
			  return $this->belongsTo('App\Models\Category', 'category_id');
			}
			
			

}
