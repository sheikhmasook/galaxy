<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
	
    use HasFactory;
	  protected $table = 'invoices';
     protected $fillable = ['category_id','sub_cat_id','title','description','product_image','price','units','quantity','weight','colour','size','material','brand'
        ];
        public function category() {
			  return $this->belongsTo('App\Models\Category', 'cat_id');
			}
			public function subcategory() {
			  return $this->belongsTo('App\Models\SubCategory', 'sub_cat_id');
			}
}
