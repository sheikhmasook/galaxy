<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
     protected $fillable = ['category_id','sub_cat_id','title','description','product_image','price','units','quantity','weight','colour','type',
        ];
        public function category() {
			  return $this->belongsTo('App\Models\Category', 'category_id');
			}
			public function subcategory() {
			  return $this->belongsTo('App\Models\SubCategory', 'sub_cat_id');
			}
}
