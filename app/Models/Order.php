<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model{
    use HasFactory;
      protected $fillable = ['user_id','product_id','discount','qty','sub_total','tax','total'
         ];
             public function category() {
			  return $this->belongsTo('App\Models\Category', 'cat_id');
			}
			public function subcategory() {
			  return $this->belongsTo('App\Models\SubCategory', 'sub_cat_id');
			}
			public function user() {
		    return $this->belongsTo('App\Models\User', 'user_id');
		}
		public function orderItem() {
		  return $this->hasMany('App\Models\OrderItem', 'order_id');
		}
		
}
