<?php
use App\Models\Setting;
use Illuminate\Support\Facades\Cache;

function settingCache($key = null) {
	if ($key) {
		$value = Cache::rememberForever($key, function () use ($key) {
			$check = Setting::where('field_name', $key)->first();
			if ($check) {
				return $check->value;
			} else {
				return '';
			}
		});
		return $value;
	} else {
		return '';
	}
}

function setting($key = null) {
	if ($key) {
		$setting = Setting::where('field_name', $key)->first();
		if ($setting) {
			return $setting->value;
		} else {
			return '';
		}
	} else {
		return '';
	}
}

function generateReferalCode($name) {
	$referral_code = strtolower(substr($name, 0, 3));
	$uid = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 5);
	return $referral_code . $uid;
}

