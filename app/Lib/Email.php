<?php
namespace App\Lib; 
use Mail;
use App\Models\EmailTemplate;  
use App\Models\Setting;  
/**
 * 
 * 
 * This Library use for image upload and resizing.
 *  
 * 
 **/

class Email
{

  public static function sendEmail($content,$email,$subject) {
        try {
           
            $from_mail      = "support@galaxymarble.in";//env('MAIL_FROM_ADDRESS');
            $site_title      = "Galaxy Marble & paint store";//env('APP_NAME');
            //print_r($from_mail);die;
            $sendmail = Mail::send([], [], function ($m) use ($from_mail, $site_title, $email, $subject, $content) {
                $m->from($from_mail, $site_title);
                $m->to($email, $email);
                $m->subject($subject);
                $m->setBody($content, 'text/html');
            });
           }catch (Exception $e) {
            return $e->getMessage();
        }

    }
}
