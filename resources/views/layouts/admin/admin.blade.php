<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>{{isset($page_title)? $page_title:config('app.name', 'Galaxy')}}</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <link rel="icon" href="{{ url('public/Admin/img/logo.png')}}" type="image/gif" sizes="32x32">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Fonts and icons -->
  <script src="{{url('public/Admin/js/plugin/webfont/webfont.min.js')}}"></script>
  <script>
    WebFont.load({
      google: {"families":["Lato:300,400,700,900"]},
      custom: {"families":["Flaticon","Font Awesome 5 Regular", "Font Awesome 5 Brands"], urls: ['{{url("public/Admin/css/fonts.min.css")}}']},
      active: function() {
        sessionStorage.fonts = true;
      }
    });
  </script>

  <!-- CSS Files -->
  <link rel="stylesheet" href="{{url('public/Admin/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('public/Admin/css/atlantis.css')}}">
  <link href="{{url('public/Admin/js/plugin/SnackBar-master/dist/snackbar.min.css')}}" rel="stylesheet">
  <link href="{{url('public/Admin/js/plugin/dropify-master/dist/css/dropify.min.css')}}" rel="stylesheet">
  <link href="{{url('public/Admin/js/plugin/summernote/summernote-bs4.css')}}" rel="stylesheet">
  @yield('styles')
</head>
<body>
  <div class="wrapper">
    @include('admin.includes.navbar')
    <!-- Sidebar -->
    @include('admin.includes.sidebar')
    <!-- End Sidebar -->
    <div class="main-panel">
      <div class="container">
        <div class="panel-header">
          <div class="page-inner">
            <div class="page-header">
            <h4 class="page-title">{{isset($page_title)?$page_title:''}}</h4>
             <?php if (isset($breadcrumbs) && count($breadcrumbs) > 0) {?>
            <ul class="breadcrumbs">

                    <li class="nav-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                        <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                  </li>
                    <?php foreach ($breadcrumbs as $breadcrumb) {?>
                        <?php if ($breadcrumb['relation'] == "link") {?>
                            <li class="nav-item"><a href="{{$breadcrumb['url']}}">{{$breadcrumb['name']}}</a></li>
                            <li class="separator">
                              <i class="flaticon-right-arrow"></i>
                            </li>
                        <?php } else {?>
                            <li class="nav-item active">{{$breadcrumb['name']}}</li>
                        <?php }?>
                    <?php }?>

            </ul>
            <?php }?>
          </div>
          </div>
        </div>
        <div class="page-inner mt--5">
          @yield('content')
        </div>
      </div>
      @include('admin.includes.footer')
    </div>
    <!-- End Custom template -->
  </div>
  <!--   Core JS Files   -->
  <script src="{{url('public/Admin/js/core/jquery.3.2.1.min.js')}}"></script>
  <script src="{{url('public/Admin/js/core/popper.min.js')}}"></script>
  <script src="{{url('public/Admin/js/core/bootstrap.min.js')}}"></script>
  <script src="{{url('public/Admin/js/form.js')}}"></script>

  <script src="{{url('public/Admin/js/plugin/SnackBar-master/dist/snackbar.min.js')}}"></script>

  <!-- jQuery Scrollbar -->
  <script src="{{url('public/Admin/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>

  <script src="{{url('public/Admin/js/plugin/dropify-master/dist/js/dropify.min.js')}}"></script>
  <!-- Moment JS -->
  <script src="{{url('public/Admin/js/plugin/moment/moment.min.js')}}"></script>
  

  <!-- Chart JS -->
  <script src="{{url('public/Admin/js/plugin/chart.js/chart.min.js')}}"></script>

  <!-- jQuery Sparkline -->
  <script src="{{url('public/Admin/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>

  <!-- Chart Circle -->
  <script src="{{url('public/Admin/js/plugin/chart-circle/circles.min.js')}}"></script>

  <!-- Datatables -->
  <script src="{{url('public/Admin/js/plugin/datatables/datatables.min.js')}}"></script>

  <!-- jQuery Vector Maps -->
  <script src="{{url('public/Admin/js/plugin/jqvmap/jquery.vmap.min.js')}}"></script>
  <script src="{{url('public/Admin/js/plugin/jqvmap/maps/jquery.vmap.world.js')}}"></script>

  <!-- Google Maps Plugin -->
  <script src="{{url('public/Admin/js/plugin/gmaps/gmaps.js')}}"></script>

  <!-- DateTimePicker -->
  <script src="{{url('public/Admin/js/plugin/datepicker/bootstrap-datetimepicker.min.js')}}"></script>

  <!-- Summernote -->
  <script src="{{url('public/Admin/js/plugin/summernote/summernote-bs4.min.js')}}"></script>

  <!-- Select2 -->
  <script src="{{url('public/Admin/js/plugin/select2/select2.full.min.js')}}"></script>

  <!-- Atlantis JS -->
  <script src="{{url('public/Admin/js/atlantis.min.js')}}"></script>
  <script src="{{url('public/Admin/js/custom.js')}}"></script>
  <script src="{{url('public/Admin/js/sweetalert.min.js')}}"></script>
  @yield('scripts')
</body>

</html>