<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Galaxy Marble</title>
<link rel="icon" href="http://galaxymarble.in/public/Admin/img/logo.png" type="image/gif" sizes="32x32">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:regular,bold"> 
 <link rel="stylesheet" type="text/css" href="{{ url('public/front/css/bootstrap.min.css')}}">
<link href="et-line-font/style.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ url('public/front/css/styles.css')}}">


</head>

<body id="home">
   @if(Session::has('danger'))
      <div class="set-notify alert alert-danger text-center alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Opps!</strong> {{ Session::get('danger') }}
      </div>
       @elseif(Session::has('success'))
      <div class="set-notify alert alert-danger text-center alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Great!</strong> {{ Session::get('success') }}
      </div>
      @endif
<section class="main">     
<div id="Content" class="wrapper topSection">
	<div id="Header">
	<div class="wrapper">
		<div class="logo"><h1><img src="{{ url('public/front/img/logo.png')}}" />Galaxy Marble</h1>	</div>
		</div>
	</div>
	<h2>Stay tuned we're launching soon.</h2> 	
<div class="countdown styled"></div>
</div>
</section>
<section class="subscribe spacing">
<div class="container">
<div class="row justify-content-center sub_box">
  <div class="col-md-5">
    <div id="subscribe">  
<h3>Subscribe To Get Notified</h3>
  {!! Form::model('subscribe',['route' => ['front.subscribe'], 'method' => 'post','autocomplete'=>'off']) !!}
    <p class="subscribe-input">
        <?= Form::text('email', null,['class' => '', 'placeholder' => 'Subscribe your e-mail Id', 'id' => 'inputEmail','required'=>true]); ?>
         <?= Form::submit('Submit', ['class' => '']) ?>
       
    </p>
    <span class=" text-danger error email">{{ $errors->first('email')}}</span>
        
  {{ Form::close() }}
  <div id="socialIcons">
    <ul> 
      <li><a href="" title="Twitter" class="twitterIcon"></a></li>
      <li><a href="" title="facebook" class="facebookIcon"></a></li>
      <li><a href="" title="linkedIn" class="linkedInIcon"></a></li>
      <li><a href="" title="Pintrest" class="pintrestIcon"></a></li>
    </ul>
  </div>
</div>
  </div>
 </div> 

 
</div>
</section>

<section class="features spacing">
  <div class="container">
    <h2 class="text-center">Features</h2>
    <div class="row">
      <div class="col-md-6">
        <div class="featuresPro">
          <div class="col-md-3 col-sm-3 col-xs-3 text-center icon"><i class="icon-gift"></i></div>
          <div class="col-md-9 col-sm-9 col-xs-9"> 
            <!--features 1-->
            <h4>Plumber</h4>
            <p>Plumber's install and repair pipes that supply water and gas to, as well as carry waste away from, homes and businesses. They also install plumbing fixtures such as bathtubs, sinks, and toilets, and appliances, including dishwashers and washing machines.</p>
            <!--features 1 end--> 
          </div>
        </div>
        <div class="featuresPro">
          <div class="col-md-3 col-sm-3 col-xs-3 text-center icon"><i class="icon-trophy"></i></div>
          <div class="col-md-9 col-sm-9 col-xs-9"> 
            <!--features 2-->
            <h4>Painter</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit, beatae, esse, aspernatur, alias odio numquam incidunt perspiciatis aliquid voluptate sapiente.</p>
            <!--features 2 end--> 
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="featuresPro">
          <div class="col-md-3 col-sm-3 col-xs-3 text-center icon"><i class="icon-tools"></i></div>
          <div class="col-md-9 col-sm-9 col-xs-9"> 
            <!--features 3-->
            <h4>Floor Tiles</h4>
            <p>Here you will get luxurious floor tiles integrates technological expertise with exquisite designs. Each tile is crafted using ultra-modern technology to ensure high quality. .</p>
            <!--features 3 end--> 
          </div>
        </div>
        <div class="featuresPro">
          <div class="col-md-3 col-sm-3 col-xs-3 text-center icon"><i class="icon-upload"></i></div>
          <div class="col-md-9 col-sm-9 col-xs-9"> 
            <!--features 4-->
            <h4>Marble</h4>
            <p>Galaxy offers the best Marble Tiles collection for your home and office spaces. Marble Tiles are gives perfect elegant & stylish look to floors & walls.</p>
            <!--features 4 end--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<span class="tempBy">&copy; 2021 Galaxy Marble.</span>

<!--Scripts-->
<script type="text/javascript" src="{{ url('public/front/js/jquery-1.9.1.min.js')}}"></script> 
<script type="text/javascript" src="{{ url('public/front/js/jquery.countdown.js')}}"></script>
<script type="text/javascript" src="{{ url('public/front/js/global.js')}}"></script>
<script type="text/javascript">
  $('.close').click(function() {
   $('.alert').hide();
})
</script>

