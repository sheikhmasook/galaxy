<?php 
$gender_list = Config::get('params.gender_list');
?>
@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
              @if(isset($getTemplate))
                {!! Form::model($getTemplate,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off')) !!}
                @else
                 {!! Form::open(['files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off']) !!}
                @endif
               
                <div class="form-group">
                    <label>Title</label>
                    {{Form::text('title',null,array('placeholder'=>'Title','id'=>'title','class'=>'form-control'))}}
                     <span class=" text-danger error title">{{ $errors->first('title')}}</span>
                </div> 
                
                <div class="form-group">
                    <label>Subject</label>
                    {!! Form::text('subject',null,array('placeholder'=>'Subject','id'=>'subject','class'=>'form-control ')) !!}
                     <span class=" text-danger error subject">{{ $errors->first('subject')}}</span>
                </div> 
                <div class="form-group">
                    <label>Keywords</label>
                    {!! Form::text('keywords',null,array('placeholder'=>'Keywords','id'=>'keywords','class'=>'form-control ')) !!}
                     <span class=" text-danger error subject">{{ $errors->first('keywords')}}</span>
                </div> 
               
                <div class="form-group">
                    <label>Content</label>
                    {!! Form::textarea('content',null,array('placeholder'=>'Enter Content Number','id'=>'content','class'=>'form-control')) !!}
                    <span class=" text-danger error content">{{ $errors->first('content')}}</span>
                </div> 
                 @if($id)
                {!! Form::button('Update',['class'=>'btn btn-primary','id'=>'submit-btn','type'=>'submit']) !!}
                @else
                {!! Form::button('Save',['class'=>'btn btn-primary','id'=>'submit-btn','type'=>'submit']) !!}
                @endif
                <a class="btn btn-secondary" href="{{route('admin.users.index')}}">Back</a> 
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function() {
  $('#content').summernote();
});
$(function(){
  $('#submit-form').ajaxForm({
   // $("#submit-btn").html('<i class="fas fa-spinner fa-spin"></i>');
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
      if(response.status=='true'){
        window.location.href = '{{route('admin.template.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 
});
</script>
@endsection
