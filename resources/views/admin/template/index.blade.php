@extends('layouts.admin.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive">
                        <table id="template-table" class="display table table-striped table-hover w-100" >
                            <thead>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Subject</th>
                                 <th>Type</th>
                                <th>Status</th>
                                <th>Created on</th>
                                <th width="100">Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('#template-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.template.datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'subject', name: 'subject'},
            {data: 'type', name: 'type'},
            {data: 'status', name: 'status',orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });
</script>
@endsection