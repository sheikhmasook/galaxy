<?php $unitss = Config::get('params.units');?>
@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
              @if(isset($getproduct))
                {!! Form::model($getproduct,array('class'=>'','id'=>'submit-form','autocomplete'=>'off')) !!}
                @else
                 {!! Form::open(['class'=>'','id'=>'submit-form','autocomplete'=>'off']) !!}
                @endif
                <div class=" p-4">
                   <div class="form-row">

                  <div class="form-group col-md-6">
                    <label>Category<em style="color:red">*</em></label>
                    {{Form::select('category_id',$category_list,null,array('placeholder'=>'Select Category','id'=>'category_id','class'=>'form-control','onchange'=>'getsubcat(this)'))}}
                     <span class=" text-danger error category_id">{{ $errors->first('category_id')}}</span>
                </div> 
                <?php $sub_category_list = ($sub_category_list) ? $sub_category_list : [];?>
                 <div class="form-group col-md-6">
                    <label>Sub Category<em style="color:red">*</em></label>
                    {{Form::select('sub_cat_id',$sub_category_list,null,array('placeholder'=>'Select Sub Category','id'=>'sub_cat_id','class'=>'form-control'))}}
                     <span class=" text-danger error sub_cat_id">{{ $errors->first('sub_cat_id')}}</span>
                </div> 

                <div class="form-group col-12">
                    <label>Product Name<em style="color:red">*</em></label>
                    {{Form::text('title',null,array('placeholder'=>'Title','id'=>'title','class'=>'form-control'))}}
                     <span class=" text-danger error title">{{ $errors->first('title')}}</span>
                </div> 
                <div class="form-group col-md-6">
                    <label>Type<em style="color:red">*</em></label>
                    {{Form::text('type',null,array('placeholder'=>'Type','id'=>'type','class'=>'form-control'))}}
                     <span class=" text-danger error type">{{ $errors->first('type')}}</span>
                </div> 
                <div class="form-group col-md-6">
                    <label>Colour<em style="color:red">*</em></label>
                    {{Form::text('colour',null,array('placeholder'=>'Colour','id'=>'colour','class'=>'form-control','data-jscolor'=>"{}"))}}
                     <span class=" text-danger error colour">{{ $errors->first('colour')}}</span>
                </div> 
                <div class="form-group col-md-6">
                    <label>Total Quantity<em style="color:red">*</em></label>
                    {{Form::text('quantity',null,array('placeholder'=>'Quantity','id'=>'quantity','class'=>'form-control'))}}
                     <span class=" text-danger error quantity">{{ $errors->first('quantity')}}</span>
                </div> 
                
                <div class="form-group col-md-6">
                    <label>Weight</label>
                    {{Form::text('weight',null,array('placeholder'=>'Weight','id'=>'weight','class'=>'form-control'))}}
                     <span class=" text-danger error weight">{{ $errors->first('weight')}}</span>
                </div> 
                <div class="form-group col-md-6">
                    <label>Unit<em style="color:red">*</em></label>
                    {{Form::select('units',$unitss,null,array('placeholder'=>'Select Unit','id'=>'units','class'=>'form-control'))}}
                     <span class=" text-danger error units">{{ $errors->first('units')}}</span>
                </div> 
                <div class="form-group col-md-6">
                    <label>Price<em style="color:red">*</em></label>
                    {{Form::text('price',null,array('placeholder'=>'Price','id'=>'price','class'=>'form-control'))}}
                     <span class=" text-danger error price">{{ $errors->first('price')}}</span>
                </div> 
                 <div class="form-group col-md-12">
                    <label>Product Image</label>
                    @if($getproduct)
                    <input type="file" id="product_image" data-default-file="{{($getproduct->product_image && file_exists($getproduct->product_image))?url($getproduct->product_image):''}}" name="product_image" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif"/>
                    @else
                    <input type="file" id="product_image" name="product_image" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif"/>
                    @endif
                    <span class=" text-danger error product_image">{{ $errors->first('product_image')}}</span>
                </div> 
                
                <div class="form-group col-12">
                    <label>Description</label>
                    {!! Form::textarea('description',null,array('placeholder'=>'Description','id'=>'description','class'=>'form-control')) !!}
                     <span class=" text-danger error description">{{ $errors->first('description')}}</span>
                </div> 
                
              </div>
            </div>
                
                {!! Form::button('Save',['class'=>'btn btn-primary set_icon','id'=>'submit-btn','type'=>'submit']) !!}
               
                <a class="btn btn-secondary" href="{{route('admin.category.index')}}">Back</a> 
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{url('public/Admin/js/jscolor.js')}}"></script>
<script>
$(function(){
   $(document).ready(function() {
  $('#description').summernote();
});
  $('#submit-form').ajaxForm({
   // $(".set_icon").html('<i class="fas fa-spinner fa-spin"></i>');
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
     // alert(response.status)
      if(response.status=='true'){
        window.location.href = '{{route('admin.product.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 
});
function getsubcat(element){
  var dataid = $(element).val();
  $.ajax({
            url: "{{route('admin.product.subcat')}}",
            type: "POST",
            data:{id:dataid},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
              if(data.status==true){
                $("#sub_cat_id").html(data.list);
              }else{
                 Alert(data.message,false);
              }
            },

        });


 }

</script>
@endsection
