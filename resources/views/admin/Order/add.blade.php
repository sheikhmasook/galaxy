<?php $unitss = Config::get('params.units');?>
@extends('layouts.admin.admin')
@section('content')
<div class="row">

    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              Add user
            </button>
              @if(isset($getProduct))
                {!! Form::model($getProduct,array('class'=>'','id'=>'submit-form','autocomplete'=>'off')) !!}
                @else
                 {!! Form::open(['class'=>'','id'=>'submit-form','autocomplete'=>'off']) !!}
                @endif
                <div class=" p-4">
                    <div class="form-group col-md-2">
                    <label>Select user<em style="color:red">*</em></label>
                    {{Form::select('user_id',$user_list,null,array('placeholder'=>'Select User','id'=>'user_id','class'=>'form-control'))}}
                   </div> 
                   <div class="form-row makeclone">

                  <div class="form-group col-md-2">
                    <label>Category<em style="color:red">*</em></label>
                    {{Form::select('cat_id[]',$category_list,null,array('placeholder'=>'Select Category','id'=>'cat_id_0','class'=>'form-control','onchange'=>'getsubcat(this)'))}}
                     <span class=" text-danger error cat_id">{{ $errors->first('cat_id')}}</span>
                </div> 
                <?php $sub_category_list = ($sub_category_list) ? $sub_category_list : [];?>
                 <div class="form-group col-md-2">
                    <label>Sub Category<em style="color:red">*</em></label>
                    {{Form::select('sub_cat_id[]',$sub_category_list,null,array('placeholder'=>'Select Sub Category','id'=>'sub_cat_id_0','class'=>'form-control','onchange'=>'getproduct(this)'))}}
                </div> 

                <?php $product_list = ($product_list) ? $product_list : [];?>
                 <div class="form-group col-md-2">
                    <label>Select Product<em style="color:red">*</em></label>
                    {{Form::select('product_id[]',$product_list,null,array('placeholder'=>'Select Product','id'=>'product_id_0','class'=>'form-control','onchange'=>'getweight(this)'))}}
                </div> 
                <div class="form-group col-2">
                    <label>Weight/Size<em style="color:red">*</em></label>
                    {{Form::select('weight[]',[],null,array('placeholder'=>'select Weight/Size','id'=>'weight_0','class'=>'form-control'))}}
                </div> 
                  <div class="form-group col-md-2">
                    <label>Qty<em style="color:red">*</em></label>
                    {{Form::text('qty[]',null,array('placeholder'=>'Enter Quantity','id'=>'qty_0','class'=>'form-control'))}}
                </div> 
                 <div class="form-group col-md-2">
                    <label>Unit<em style="color:red">*</em></label>
                    {{Form::select('units[]',$unitss,null,array('placeholder'=>'Select Unit','id'=>'units_0','class'=>'form-control','onchange'=>'getprice(this)'))}}
                </div> 
               
                 <div class="form-group col-md-2">
                    <label>Price<em style="color:red">*</em></label>
                    {{Form::text('price[]',null,array('placeholder'=>'Enter Price','id'=>'price_0','class'=>'form-control','readonly'=>'readonly'))}}
                </div> 

                {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>',['class'=>'btn btn-primary','id'=>'rmovemenu','type'=>'button','onclick' => 'removemenu(this)','style' => "display:none"]) !!}
              </div>
              {!! Form::button('Add',['class'=>'btn btn-success set_icon','id'=>'add-btn','type'=>'button','onclick' => 'addmoremenu(this)','style'=>'float:right']) !!}
               
            </div>

                
                {!! Form::button('Save',['class'=>'btn btn-primary set_icon','id'=>'submit-btn','type'=>'submit']) !!}

                
               
                <a class="btn btn-secondary" href="{{route('admin.order.index')}}">Back</a> 
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add user</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
 <form id='submit-user-form' method="post">
      <div class="modal-body">
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" class="form-control" id="name"  placeholder="Enter Name" name="name">  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Mobile Number</label>
    <input type="number" class="form-control" id="phone_no" placeholder="Phone No" name="phone_no">
  </div>
      </div>
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="submit-modal">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.4.5/jscolor.min.js"></script>
<script>
$(function(){
   $(document).ready(function() {
  $('#description').summernote();
   $('#user_id').select2();
});
   $("#submit-form").on('submit',function(e){
    e.preventDefault();
    $("#submit-btn").html('<i class="fas fa-spinner fa-spin"></i>');
    $(".error").remove();
        $.ajax({
            url: "{{route('admin.order.add')}}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
              $("#submit-btn").html('Save');
              if(data.status=='true'){
                  window.location.href = "{{route('admin.order.index')}}";
              }else{
                alert(data.message,false);
              }
            },
            error: function (data) {
             $("#submit-btn").html('Save');
                if(data.status==500){
                  alert(data.message,false);
                }
              var response = JSON.parse(data.responseText);
              $.each(response.errors, function (k, v) {
                   str = v.toString();
                   v = str.replace(k, k.split('.')[0]);
                   k = k.replace(".", "_");
                  $("#" + k).after("<span class='text-danger error'>"+v+"</span>");
              });
            }
        });
  });

});
var random = 1;
function addmoremenu(){
       appendElement = $(".makeclone").first().clone();
        var len = $(".makeclone").length;
       appendElement.find('select').eq(0).attr('id','cat_id_'+len);
       appendElement.find('select').eq(1).attr('id','sub_cat_id_'+len);
       appendElement.find('select').eq(2).attr('id','product_id_'+len);
       appendElement.find('select').eq(3).attr('id','weight_'+len);
       appendElement.find('input').eq(4).attr('id','qty_'+len);
       appendElement.find('select').eq(5).attr('id','units_'+len);
       appendElement.find('input').eq(6).attr('id','price_'+len);
       lastElement = $(".makeclone").last();
       var selectedElement = '';
       selectedElement = $(appendElement).insertAfter(lastElement);
       rando = random + 1;
     //  $(selectedElement).find('label').text(rando);
       $(selectedElement).find('#rmovemenu').show();
       $(selectedElement).find('input').val('');
       $(selectedElement).find('select').val('');
       $(selectedElement).find("span").remove();

random++;}
function removemenu(elemnt){
   $(elemnt).parent().remove();

}
function getsubcat(element){
  var dataid = $(element).val();
  var id = $(element).attr('id');
  var splitdata = id.split("_").pop();
  // alert(splitdata);
  $.ajax({
            url: "{{route('admin.order.subcat')}}",
            type: "POST",
            data:{id:dataid},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
              if(data.status==true){
                $("#sub_cat_id_"+splitdata).html(data.list);
               // getproduct();
              }else{
                 Alert(data.message,false);
              }
            },

        });


 }
 function getproduct(element){
  var subcat_id = $(element).val();
  //  if(id !=''){
  //   var subcat_id = $(element).val();
  //  }else{}
  
  var id = $(element).attr('id');
  var splitdata = id.split("_").pop();
  $.ajax({
            url: "{{route('admin.order.product')}}",
            type: "POST",
            data:{subcat_id:subcat_id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
              if(data.status==true){
                $("#product_id_"+splitdata).html(data.list);
                 $("#weight_"+splitdata).html(data.weight_list);
              }else{
                 Alert(data.message,false);
              }
            },

        });
 }
  function getprice(element){
  var id = $(element).attr('id');
  var splitdata = id.split("_").pop();
  var cat_id = $("#cat_id_"+splitdata).val();
  var sub_cat_id = $("#sub_cat_id_"+splitdata).val();
  var product_id = $("#product_id_"+splitdata).val();
  var weight = $("#weight_"+splitdata).val();
  var unit_id = $(element).val();
  $.ajax({
            url: "{{route('admin.order.product-price')}}",
            type: "POST",
            data:{cat_id:cat_id,sub_cat_id:sub_cat_id,product_id:product_id,weight:weight,unit_id:unit_id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
              if(data.status==true){
                $("#price_"+splitdata).val(data.price);
              }else{
                 Alert(data.message,false);
              }
            },

        });
 }
 function getweight(element){
  var id = $(element).attr('id');
  var splitdata = id.split("_").pop();
  var product_id = $("#product_id_"+splitdata).val();
  $.ajax({
            url: "{{route('admin.order.product-wight')}}",
            type: "POST",
            data:{product_id:product_id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
              if(data.status==true){
                $("#weight_"+splitdata).html(data.list);
              }else{
                 Alert(data.message,false);
              }
            },

        });
 }
$("#submit-user-form").on('submit',function(e){
    e.preventDefault();
    $("#submit-modal").html('<i class="fas fa-spinner fa-spin"></i>');
    $(".error").remove();
        $.ajax({
            url: "{{route('admin.users.add')}}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
              $("#submit-modal").html('Save');
              if(data.status=='true'){
                  window.location.href = "{{route('admin.order.add')}}";
              }else{
                alert(data.message,false);
              }
            },
            error: function (data) {
             $("#submit-btn").html('Save');
                if(data.status==500){
                  alert(data.message,false);
                }
              var response = JSON.parse(data.responseText);
              $.each(response.errors, function (k, v) {
                  $("#" + k).after("<span class='text-danger error'>"+v+"</span>");
              });
            }
        });
  });

</script>
@endsection
