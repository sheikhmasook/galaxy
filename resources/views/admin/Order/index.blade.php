@extends('layouts.admin.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive">
                        <table id="users-table" class="display table table-striped table-hover w-100" >
                            <thead>
                                <th>Id</th>
                                <th>User Name</th>
                                <th>Amount</th>
                                <th>Created on</th>
                                <th width="100">Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.order.datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'user.name', name: 'user.name'},
            {data: 'total', name: 'total'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });
</script>
@endsection