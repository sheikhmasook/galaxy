@extends('layouts.admin.invoice')
@section('content')

		<div class="page-inner">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-10 col-xl-9">
					<div class="row align-items-center">
						<div class="col">
							<h6 class="page-pretitle">
								Payments
							</h6>
							<h4 class="page-title">Invoice #FDS9876KD</h4>
						</div>
						<div class="col-auto">
							<a href="{{ route('admin.invoice.download',$order->id) }}" class="btn btn-info ml-2">
								Download
							</a>
							<a href="{{ route('admin.invoice.print',$order->id) }}" class="btn btn-primary ml-2">
								Print
							</a>
							<a class="btn btn-success ml-2" href="https://wa.me/{{'91'.$order->user->phone_no}}/?text=Welcome to galaxy Marble and paint store.this is a general invoice,so please check it accordingly, which we have also attached invoice file along this message." target="_blank">
							<span class="icon-whatsapp-share"></span>Share on Whatsapp</a></li>

							</div>
						</div>
						<div class="page-divider"></div>
						<div class="row">
							<div class="col-md-12">
								<div class="card card-invoice">
									<div class="card-header">
										<div class="invoice-header">
											<h3 class="invoice-title">
												Invoice
											</h3>
											<div class="invoice-logo">
												<img src="{{ url('public/Admin/img/logo.png')}}" alt="company logo" style="height: 70px;width: 70px">
											</div>
										</div>
										<div class="invoice-desc">
											Khakhreru Jawahar nagar 212656.<br/>
											Mob. 9079124453
										</div>
									</div>
									<div class="card-body">
										<div class="separator-solid"></div>
										<div class="row">
											<div class="col-md-4 info-invoice">
												<h5 class="sub">Date</h5>
												<p>{{ date('M d,Y',strtotime($order->created_at))}}</p>
											</div>
											<div class="col-md-4 info-invoice">
												<h5 class="sub">Invoice ID</h5>
												<p>#GMS9876KD</p>
											</div>
											<div class="col-md-4 info-invoice">
												<h5 class="sub">Invoice To</h5>
												<p>
													Jane Smith, 1234 Main, Apt. 4B<br/>Springfield, ST 54321 
												</p>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="invoice-detail">
													<div class="invoice-top">
														<h3 class="title"><strong>Order summary</strong></h3>
													</div>
													<div class="invoice-item">
														<div class="table-responsive">
															<table class="table table-striped">
																<thead>
																	<tr>
																		<td><strong>Item</strong></td>
																		<td class="text-center"><strong>Price</strong></td>
																		<td class="text-center"><strong>Weight</strong></td>
																		<td class="text-center"><strong>Unit</strong></td>
																		<td class="text-center"><strong>Quantity</strong></td>
																		<td class="text-right"><strong>Totals</strong></td>
																	</tr>
																</thead>
																<tbody>
																	@foreach ($order['orderItem'] as $key => $value) 
																	<tr>
																		<td>{{ $value->product->title }}</td>
																		<td class="text-center">{{ '₹'.$value->price }}</td>
																		<td class="text-center">{{ $value->weight }}</td>
																		<td class="text-center">{{ $value->unit }}</td>
																		<td class="text-center">{{ $value->qty }}</td>
																		<td class="text-right">{{ '₹'.$value->price * $value->qty }}</td>
																	</tr>
																	@endforeach
																</tbody>
															</table>
														</div>
													</div>
												</div>	
												<div class="separator-solid  mb-3"></div>
											</div>	
										</div>
									</div>
									<div class="card-footer">
										<div class="row">
											<div class="col-sm-7 col-md-5 mb-3 mb-md-0 transfer-to">
												<h5 class="sub" align="center">Signature:</h5>
												<textarea id="sign" rows="1" cols="25" readonly="readonly"></textarea>
											</div>
											<div class="col-sm-5 col-md-7 transfer-total">
												<h5 class="sub">Total Amount</h5>
												<div class="price">{{ '₹'.$order->total }}</div>
												<span>Taxes Included</span>
											</div>
										</div>
										<div class="separator-solid"></div>
										<h6 class="text-uppercase mt-4 mb-3 fw-bold">
											Notes
										</h6>
										<p class="text-muted mb-0">
											We really appreciate your business and if there's anything else we can do, please let us know! Also, should you need us to add VAT or anything else to this order, it's super easy since this is a template, so just ask!
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	@endsection
