@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">

                    <hr>
                    <div style="text-align:center">
                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>User Name</strong>
                                <br>
                                <p class="text-muted">{{@($order->user)?$order->user->name:'N/A'}}</p>
                            </div>
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Transaction id</strong>
                                <br>
                                <p class="text-muted"><?=isset($order->order->transaction_id) ? $order->order->transaction_id : '--'?></p>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong> Order Date</strong>
                                <br>
                                <p class="text-muted"><?php echo date('d M Y', strtotime($order->created_at)) ?></p>
                            </div>
                                <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong> Status</strong>
                                <br>
                                <p class="text-muted"><?php echo ($order->status== 1) ? "Ready for delivery":"Pending" ?></p>
                            </div>

                        </div>






                        <table class="a_table">
                          <tr>
                            <td><strong>Item</strong></td>
                            <td class="text-center"><strong>Price</strong></td>
                            <td class="text-center"><strong>Weight</strong></td>
                            <td class="text-center"><strong>Unit</strong></td>
                            <td class="text-center"><strong>Quantity</strong></td>
                            <td class="text-right"><strong>Totals</strong></td>
                        </tr>
                        <tr>
                              @foreach ($order['orderItem'] as $key => $value) 
                                    <tr>
                                        <td>{{ $value->product->title }}</td>
                                        <td class="text-center">{{ '₹'.$value->price }}</td>
                                        <td class="text-center">{{ $value->weight }}</td>
                                        <td class="text-center">{{ $value->unit }}</td>
                                        <td class="text-center">{{ $value->qty }}</td>
                                        <td class="text-right">{{ '₹'.$value->price * $value->qty }}</td>
                                    </tr>
                                    @endforeach
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align: right;border: none;"><strong>Sub Amount:</strong></td>
                            <td colspan="5" >{{ '₹'.$order->total }}</td>
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align: right;border: none;"><strong>Tax:</td>
                                <td colspan="5">{{  '₹'.$order->tax }}</td>
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align: right;border: none;"><strong>Discount:</strong></td>
                            <td colspan="5">{{  '₹'.$order->discount }}</td>
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align: right;border: none;"><strong>Total: </strong>
                                <br>(Inclusive of 18% vat)</td>
                            <td colspan="5">{{ '₹'.$order->total }}</td>
                        </tr>

                    </table>
</br>
                    <div class="row">
                        <div class="col-md-12">

                            <a class="btn btn-success" href="javascript:void(0)" onclick="goBack()">Go Back</a>
                        </div>
                    </div>
                </div>
                <div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
