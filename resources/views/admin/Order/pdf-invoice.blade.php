<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Galaxy Marble & Paint store</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		body{
			width: 100%;
			box-shadow: 0 0 4px rgb(0 0 0 / 16%);
			padding: 15px;
			margin: 0 auto;
			border-radius: 5px;
		}
		td, th {
				text-align: left;
			padding: 8px;
		}
		table{
			width: 100%;
			border-collapse: collapse;
			 margin-bottom: 0px; 
			border-bottom: solid 1px #ddd;
			padding: 26px 0;
		}
		table th, td {
			padding: 5px 0 10px; 
		}
		.order_summ tr:nth-child(even){
			background-color: rgba(0,0,0,.05);
		}
		.order_summ td{
			border: solid 1px #ddd;
			padding: 10px;
		}
		.order_summ th{
			border: solid 1px #ddd;
			padding: 10px;
		}
	</style>
</head>
<body>

	<table>
		<tr>
			<td  style="font-size: 24px; width: 50%;">Invoice</td>
			<td style="text-align: center; width: 50%;"><img src="{{ url('public/Admin/img/logo.png')}}"  style="width: 70px; height: 70px;"></td>
		</tr>
		<tr>
			<td style="width: 50%;"></td>
			<td style="font-size: 16px; text-align: right;">Khakhreru Jawahar nagar 212656. <br>
			Mob. 9079124453</td>
		</tr>

	</table>
	<table>

		<thead>

			<th style="width: 33%;text-align: left;">Date</th>
			<th style="width: 33%;text-align: left;">Invoice ID</th>
			<th style="width: 33%;text-align: left;">Invoice To</th>
		</thead>
		<tbody>
			<tr>
				<td style="text-align: left;">{{ date('M d,Y',strtotime($order->created_at))}}</td>
				<td style="text-align: left;">#GMS9876KD</td>
				<td style="text-align: left;">Jane Smith, 1234 Main, Apt. 4B Springfield, ST 54321</td>
			</tr>
		</tbody>
	</table>
	<table class="order_summ">
		<h4>Order Summry</h4>
		<thead>

			<th style="text-align: center;">Item</th>
			<th style="text-align: center;">Price</th>
			<th style="text-align: center;">Weight</th>
			<th style="text-align: center;">Unit</th>
			<th style="text-align: center;">Quantity</th>
			<th style="text-align: center;">Totals</th>
		</thead>
		<tbody>
            @foreach ($order['orderItem'] as $key => $value) 
			<tr>
				<td style="text-align: center;">{{ $value->product->title }}</td>
				<td style="text-align: center; font-family: DejaVu Sans; sans-serif;">&#8377;{{ $value->price }}</td>
				<td style="text-align: center;">{{ $value->weight }}</td>
				<td style="text-align: center;">{{ $value->unit }}</td>
				<td style="text-align: center;">{{ $value->qty }}</td>
				<td style="text-align: center;font-family: DejaVu Sans; sans-serif;">&#8377;{{ $value->price * $value->qty }}</td>
			</tr>
			 @endforeach
		</tbody>
	</table>

	<table>
		<thead>
			<th style="width: 50%; text-align: center;">Signature:</th>
			<th style="text-align: right;">Grand Total (Incl.Tax):</th>
		</thead>
		<tr>
			<td  style="font-size: 24px; width: 50%; text-align: center;"><textarea rows="2"></textarea></td>
			<td style="text-align: right;font-size: 28px;color: #4e35f9;font-weight: bold;font-family: DejaVu Sans; sans-serif;">&#8377;{{ $order->total }} <br></td>
		</tr>

	</table>

	<table>
		<tr>
			<td style="font-size: 18px;font-weight: bold;">Note</td>
		</tr>
		<tr>
			<td style="font-size: 16px;">We really appreciate your business and if there's anything else we can do, please let us know! Also, should you need us to add VAT or anything else to this order, it's super easy since this is a template, so just ask!</td>
		</tr>
		<tr>
			<td style="font-size: 25px; text-align: center;font-weight: bold;color: #4e35f9;">Have a nice day.</td>
		</tr>
	</table>

</body>
</html>
