<?php $unitss = Config::get('params.units');?>
@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
              @if(isset($getinvoice))
                {!! Form::model($getinvoice,array('class'=>'','id'=>'submit-form','autocomplete'=>'off')) !!}
                @else
                 {!! Form::open(['class'=>'','id'=>'submit-form','autocomplete'=>'off']) !!}
                @endif
                <div class=" p-4">
                   <div class="form-row makeclone">

                  <div class="form-group col-md-2">
                    <label>Category<em style="color:red">*</em></label>
                    {{Form::select('cat_id[]',$category_list,null,array('placeholder'=>'Select Category','id'=>'cat_id','class'=>'form-control','onchange'=>'getsubcat(this)'))}}
                     <span class=" text-danger error cat_id">{{ $errors->first('cat_id')}}</span>
                </div> 
                <?php $sub_category_list = ($sub_category_list) ? $sub_category_list : [];?>
                 <div class="form-group col-md-2">
                    <label>Sub Category<em style="color:red">*</em></label>
                    {{Form::select('sub_cat_id[]',$sub_category_list,null,array('placeholder'=>'Select Sub Category','id'=>'sub_cat_id','class'=>'form-control','onchange'=>'getproduct(this)'))}}
                     <span class=" text-danger error sub_cat_id">{{ $errors->first('sub_cat_id')}}</span>
                </div> 

                <?php $product_list = ($product_list) ? $product_list : [];?>
                 <div class="form-group col-md-2">
                    <label>Select Product<em style="color:red">*</em></label>
                    {{Form::select('product_id[]',$product_list,null,array('placeholder'=>'Select Product','id'=>'product_id','class'=>'form-control'))}}
                     <span class=" text-danger error product_id">{{ $errors->first('product_id')}}</span>
                </div> 
                 <div class="form-group col-md-2">
                    <label>Qty<em style="color:red">*</em></label>
                    {{Form::text('qty[]',null,array('placeholder'=>'Enter Quantity','id'=>'qty','class'=>'form-control'))}}
                     <span class=" text-danger error qty">{{ $errors->first('qty')}}</span>
                </div> 
                 <div class="form-group col-2">
                    <label>Weight/Size<em style="color:red">*</em></label>
                    {{Form::text('weight[]',null,array('placeholder'=>'Enter Weight/Size','id'=>'weight','class'=>'form-control'))}}
                     <span class=" text-danger error weight">{{ $errors->first('weight')}}</span>
                </div> 
                <div class="form-group col-md-2">
                    <label>Unit<em style="color:red">*</em></label>
                    {{Form::select('units[]',$unitss,null,array('placeholder'=>'Select Unit','id'=>'units','class'=>'form-control'))}}
                     <span class=" text-danger error units">{{ $errors->first('units')}}</span>
                </div> 
                 <div class="form-group col-md-2">
                    <label>Price<em style="color:red">*</em></label>
                    {{Form::text('price[]',null,array('placeholder'=>'Enter Price','id'=>'price','class'=>'form-control','readonly'=>'readonly'))}}
                     <span class=" text-danger error price">{{ $errors->first('price')}}</span>
                </div> 

                {!! Form::button('Delete',['class'=>'btn btn-primary set_icon','id'=>'rmovemenu','type'=>'button','onclick' => 'removemenu(this)','style' => "display:none"]) !!}
              </div>
              {!! Form::button('Add',['class'=>'btn btn-success set_icon','id'=>'add-btn','type'=>'button','onclick' => 'addmoremenu(this)','style'=>'float:right']) !!}
               
            </div>

                
                {!! Form::button('Save',['class'=>'btn btn-primary set_icon','id'=>'submit-btn','type'=>'submit']) !!}
               
                <a class="btn btn-secondary" href="{{route('admin.category.index')}}">Back</a> 
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.4.5/jscolor.min.js"></script>
<script>
$(function(){
   $(document).ready(function() {
  $('#description').summernote();
});
  $('#submit-form').ajaxForm({
   // $(".set_icon").html('<i class="fas fa-spinner fa-spin"></i>');
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
     // alert(response.status)
      if(response.status=='true'){
        window.location.href = '{{route('admin.invoice.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 
});
var random = 1;
function addmoremenu(){
   appendElement = $(".makeclone").first().clone();
       lastElement = $(".makeclone").last();
       var selectedElement = '';
       selectedElement = $(appendElement).insertAfter(lastElement);
       rando = random + 1;
     //  $(selectedElement).find('label').text(rando);
       $(selectedElement).find('#rmovemenu').show();
       $(selectedElement).find('input').val('');

random++;}
function removemenu(elemnt){
   $(elemnt).parent().remove();

}
function getsubcat(element){
  var dataid = $(element).val();
  $.ajax({
            url: "{{route('admin.invoice.subcat')}}",
            type: "POST",
            data:{id:dataid},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
              if(data.status==true){
                $("#sub_cat_id").html(data.list);
              }else{
                 Alert(data.message,false);
              }
            },

        });


 }
 function getproduct(element){
  var subcat_id = $(element).val();
  $.ajax({
            url: "{{route('admin.invoice.product')}}",
            type: "POST",
            data:{subcat_id:subcat_id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
              if(data.status==true){
                $("#product_id").html(data.list);
              }else{
                 Alert(data.message,false);
              }
            },

        });
 }

</script>
@endsection
