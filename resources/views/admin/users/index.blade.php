@extends('layouts.admin.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive">
                        <table id="users-table" class="display table table-striped table-hover w-100" >
                            <thead>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                 <th>Type</th>
                                <th>Status</th>
                                <th>Created on</th>
                                <th width="100">Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.users.datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'type', name: 'type'},
            {data: 'status', name: 'status',orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });
 function deleteuser(id){
      if(id != ""){
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this record!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

            $.ajax({
              url: '{{route('admin.users.delete')}}',
              type: 'POST',
              data:{id:id},
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function (response) {
                swal("Poof! Your user record has been deleted!", {
                  icon: "success",
                });
                $("#delete_"+id).parents("tr").remove();

              }

            });
          }else {
            swal("Your user record is safe!");
          }
        });
      }
    }
</script>
@endsection