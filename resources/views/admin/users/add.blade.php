<?php 
$gender_list = Config::get('params.gender_list');
?>
@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
              @if(isset($getuser))
                {!! Form::model($getuser,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off')) !!}
                @else
                 {!! Form::open(['files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off']) !!}
                @endif
                <div class=" p-4">
                   <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Name<em style="color:red">*</em></label>
                    {{Form::text('name',null,array('placeholder'=>'Name','id'=>'name','class'=>'form-control'))}}
                     <span class=" text-danger error name">{{ $errors->first('name')}}</span>
                </div> 
                
                <div class="form-group col-md-6">
                    <label>Email<em style="color:red">*</em></label>
                    {!! Form::text('email',null,array('placeholder'=>'Email','id'=>'email','class'=>'form-control')) !!}
                     <span class=" text-danger error email">{{ $errors->first('email')}}</span>
                </div> 
               
                <div class="form-group col-md-6">
                    <label>Phone Number<em style="color:red">*</em></label>
                    {!! Form::text('phone_no',null,array('placeholder'=>'Phone Number','id'=>'phone_no','class'=>'form-control')) !!}
                    <span class=" text-danger error phone_no">{{ $errors->first('phone_no')}}</span>
                </div> 
                
                <div class="form-group col-md-6">
                    <label>Gender<em style="color:red">*</em></label>
                    {!! Form::select('gender',$gender_list,null,array('placeholder'=>'Select Gender','id'=>'gender','class'=>'form-control')) !!}
                    <span class=" text-danger error gender">{{ $errors->first('gender')}}</span>
                </div> 
                 
                <div class="form-group col-md-12">
                    <label>Image</label>
                    @if($id)
                    <input type="file" id="profile_picture" data-default-file="{{($getuser->profile_picture && file_exists($getuser->profile_picture))?url($getuser->profile_picture):''}}" name="profile_picture" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif"/>
                    @else
                    <input type="file" id="image" name="profile_picture" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif"/>
                    @endif
                    <span class=" text-danger error profile_picture">{{ $errors->first('profile_picture')}}</span>
                </div> 
                
              </div>
            </div>
                
                {!! Form::button('Save',['class'=>'btn btn-primary set_icon','id'=>'submit-btn','type'=>'submit']) !!}
               
                <a class="btn btn-secondary" href="{{route('admin.users.index')}}">Back</a> 
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$(function(){
  $('#submit-form').ajaxForm({
   // $(".set_icon").html('<i class="fas fa-spinner fa-spin"></i>');
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
     // alert(response.status)
      if(response.status=='true'){
        window.location.href = '{{route('admin.users.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 
});
</script>
@endsection
