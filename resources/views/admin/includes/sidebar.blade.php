<?php
$action = app('request')->route()->getAction();
$controller = class_basename($action['controller']);
// print_r($controller);
// print_r($action);die;
list($controller, $action) = explode('@', $controller);
?>
<div class="sidebar sidebar-style-2">
  <div class="sidebar-wrapper scrollbar scrollbar-inner">
    <div class="sidebar-content">
      <div class="user">
        @if (isset(Auth::guard('admin')->user()->profile_picture) && file_exists(Auth::guard('admin')->user()->profile_picture))
        <div class="avatar-sm float-left mr-2">
          <img src="{{url(Auth::guard('admin')->user()->profile_picture)}}" alt="..." class="avatar-img rounded-circle">
        </div>
        @else
        <div class="avatar-sm float-left mr-2">
          <img src="{{ url('public/admin/img/default-user.png') }}" alt="..." class="avatar-img rounded-circle">
        </div>
        @endif
        <div class="info">
          <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
            <span>
              <?php echo (Auth::guard('admin')->user()->name); ?>
              <span class="user-level">Administrator</span>
              <span class="caret"></span>
            </span>
          </a>
          <div class="clearfix"></div>
          <div class="collapse in" id="collapseExample">
            <ul class="nav">
              <li>
                <a href="{{route('admin.profile')}}">
                  <span class="link-collapse">My Profile</span>
                </a>
              </li>
              <li>
                <a href="{{route('admin.logout')}}">
                  <span class="link-collapse">Logout</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <ul class="nav nav-primary">
        <?php $active = ($controller == "UsersController" && in_array($action, ['dashboard'])) ? "active" : "";?>
        <li class="nav-item {{$active}}">
          <a href="{{route('admin.dashboard')}}">
            <i class="fas fa-tachometer-alt"></i>
            <p>Dashboard</p>
          </a>
        </li>



        <!-- users Management -->
        <?php $active = ($controller == "UsersController" && in_array($action, ['index','add','view'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#user">
            <i class="fas fa-user"></i>
            <p>User Management</p>
            <span class="caret"></span>
          </a>
          <?php $div_active = ($controller == "UsersController" && in_array($action, ['index','view','add'])) ? "show" : "";?>
          <div class="collapse {{ $div_active }}" id="user">
            <ul class="nav nav-collapse">
              <?php $li_active = ($controller == "UsersController" && in_array($action, ['index','view'])) ? "active" : "";?>
              <li class="{{ $li_active }}">
                <a href="{{ route('admin.users.index') }}">
                  <span class="sub-item">List of Users</span>
                </a>
              </li>
              <?php $li_active2 = ($controller == "UsersController" && in_array($action, ['add'])) ? "active" : "";?>
              <li class="{{ $li_active2 }}">
                <a href="{{ route('admin.users.add') }}">
                  <span class="sub-item">Create User</span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <!-- Category module -->

        <?php $active = ($controller == "CategoryController" && in_array($action, ['index','add'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#category">
            <i class="fas fa-list"></i>
            <p>Category Management</p>
            <span class="caret"></span>
          </a>
          <?php $div_active = ($controller == "CategoryController" && in_array($action, ['index','add'])) ? "show" : "";?>
          <div class="collapse {{ $div_active }}" id="category">
            <ul class="nav nav-collapse">
              <?php $li_active = ($controller == "CategoryController" && in_array($action, ['index'])) ? "active" : "";?>
              <li class="{{ $li_active }}">
                <a href="{{ route('admin.category.index') }}">
                  <span class="sub-item">List of Category</span>
                </a>
              </li>
              <?php $li_active2 = ($controller == "CategoryController" && in_array($action, ['add'])) ? "active" : "";?>
              <li class="{{ $li_active2 }}">
                <a href="{{ route('admin.category.add') }}">
                  <span class="sub-item">Create Category</span>
                </a>
              </li>
            </ul>
          </div>
        </li>

         <!-- Sub Category module -->

        <?php $active = ($controller == "SubCategoryController" && in_array($action, ['index','add'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#sub-category">
            <i class="fas fa-list"></i>
            <p>Sub Category Management</p>
            <span class="caret"></span>
          </a>
          <?php $div_active = ($controller == "SubCategoryController" && in_array($action, ['index','add'])) ? "show" : "";?>
          <div class="collapse {{ $div_active }}" id="sub-category">
            <ul class="nav nav-collapse">
              <?php $li_active = ($controller == "SubCategoryController" && in_array($action, ['index'])) ? "active" : "";?>
              <li class="{{ $li_active }}">
                <a href="{{ route('admin.sub-category.index') }}">
                  <span class="sub-item">List of Sub Category</span>
                </a>
              </li>
              <?php $li_active2 = ($controller == "SubCategoryController" && in_array($action, ['add'])) ? "active" : "";?>
              <li class="{{ $li_active2 }}">
                <a href="{{ route('admin.sub-category.add') }}">
                  <span class="sub-item">Create Sub Category</span>
                </a>
              </li>
            </ul>
          </div>
        </li>

        <!-- Product module -->

        <?php $active = ($controller == "ProductController" && in_array($action, ['index','add'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#product">
            <i class="fas fa-list"></i>
            <p>Product Management</p>
            <span class="caret"></span>
          </a>
          <?php $div_active = ($controller == "ProductController" && in_array($action, ['index','add'])) ? "show" : "";?>
          <div class="collapse {{ $div_active }}" id="product">
            <ul class="nav nav-collapse">
              <?php $li_active = ($controller == "ProductController" && in_array($action, ['index'])) ? "active" : "";?>
              <li class="{{ $li_active }}">
                <a href="{{ route('admin.product.index') }}">
                  <span class="sub-item">List of Product</span>
                </a>
              </li>
              <?php $li_active2 = ($controller == "ProductController" && in_array($action, ['add'])) ? "active" : "";?>
              <li class="{{ $li_active2 }}">
                <a href="{{ route('admin.product.add') }}">
                  <span class="sub-item">Create Product</span>
                </a>
              </li>
            </ul>
          </div>
        </li>

        
         <!-- Invoice module -->

        <?php $active = ($controller == "OrderController" && in_array($action, ['index','add','invoice','view'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#order">
            <i class="fas fa-file-invoice"></i>
            <p>Order Management</p>
            <span class="caret"></span>
          </a>
          <?php $div_active = ($controller == "OrderController" && in_array($action, ['index','add','invoice','view'])) ? "show" : "";?>
          <div class="collapse {{ $div_active }}" id="order">
            <ul class="nav nav-collapse">
              <?php $li_active = ($controller == "OrderController" && in_array($action, ['index','invoice','view'])) ? "active" : "";?>
              <li class="{{ $li_active }}">
                <a href="{{ route('admin.order.index') }}">
                  <span class="sub-item">List of Order</span>
                </a>
              </li>
              <?php $li_active2 = ($controller == "OrderController" && in_array($action, ['add'])) ? "active" : "";?>
              <li class="{{ $li_active2 }}">
                <a href="{{ route('admin.order.add') }}">
                  <span class="sub-item">Create Order</span>
                </a>
              </li>
            </ul>
          </div>
        </li>

        <!-- subcriber module -->
     <?php $active = ($controller == "SubscriberController" && in_array($action, ['index'])) ? "active" : "";?>
        <li class="nav-item {{$active}}">
          <a href="{{route('admin.subscriber.index')}}">
            <i class="fas fa-tachometer-alt"></i>
            <p>Subcriber Management</p>
          </a>
        </li>



        <!-- Template module -->
        <?php $active = ($controller == "TemplateController" && in_array($action, ['index','add'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#tables">
           <i class="fas fa-envelope"></i>
           <p>Template Management</p>
           <span class="caret"></span>
         </a>
         <?php $div_active = ($controller == "TemplateController" && in_array($action, ['index','add'])) ? "show" : "";?>
         <div class="collapse {{ $div_active }}" id="tables">
          <ul class="nav nav-collapse">
            <?php $li_active = ($controller == "TemplateController" && in_array($action, ['index'])) ? "active" : "";?>
            <li class="{{ $li_active }}">
              <a href="{{ route('admin.template.index') }}">
                <span class="sub-item">List of Templates</span>
              </a>
            </li>
            <?php $li_active2 = ($controller == "TemplateController" && in_array($action, ['add'])) ? "active" : "";?>
            <li class="{{ $li_active2 }}">
              <a href="{{ route('admin.template.add') }}">
                <span class="sub-item">Create Template</span>
              </a>
            </li>
          </ul>
        </div>
      </li>


        <!-- unit module -->

        <?php $active = ($controller == "UnitController" && in_array($action, ['index','add'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#unit">
            <i class="fa fa-balance-scale" aria-hidden="true"></i>
            <p>Unit Management</p>
            <span class="caret"></span>
          </a>
          <?php $div_active = ($controller == "UnitController" && in_array($action, ['index','add'])) ? "show" : "";?>
          <div class="collapse {{ $div_active }}" id="unit">
            <ul class="nav nav-collapse">
              <?php $li_active = ($controller == "UnitController" && in_array($action, ['index'])) ? "active" : "";?>
              <li class="{{ $li_active }}">
                <a href="{{ route('admin.unit.index') }}">
                  <span class="sub-item">List of Unit</span>
                </a>
              </li>
              <?php $li_active2 = ($controller == "UnitController" && in_array($action, ['add'])) ? "active" : "";?>
              <li class="{{ $li_active2 }}">
                <a href="{{ route('admin.unit.add') }}">
                  <span class="sub-item">Create Unit</span>
                </a>
              </li>
            </ul>
          </div>
        </li>

      <!-- CMS module -->

        <?php $active = ($controller == "PageController" && in_array($action, ['index','add','view'])) ? "active" : "";?>
        <li class="nav-item {{$active}}">
          <a href="{{route('admin.page.index')}}">
            <i class="fas fa-file"></i>
            <p>CMS Management</p>
          </a>
        </li>

         <!-- Faq module -->

        <?php $active = ($controller == "FaqController" && in_array($action, ['index','add','view'])) ? "active" : "";?>
        <li class="nav-item {{$active}}">
          <a href="{{route('admin.faq.index')}}">
          <i class="fa fa-question-circle" aria-hidden="true"></i>
            <p>FAQ Management</p>
          </a>
        </li>
       


    </ul>
  </div>
</div>
</div>