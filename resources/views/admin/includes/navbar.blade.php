<div class="main-header">
  <!-- Logo Header -->
  <div class="logo-header" data-background-color="blue">
    
    <a href="{{route('admin.dashboard')}}" class="logo">
      <img src="{{ url('public/Admin/img/logo.png')}}" alt="Admin Logo" class="navbar-brand" height="60px" width="80px">
    </a>
    <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon">
      <i class="icon-menu"></i>
    </span>
    </button>
    <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
    <div class="nav-toggle">
      <button class="btn btn-toggle toggle-sidebar">
      <i class="icon-menu"></i>
      </button>
    </div>
  </div>
  <!-- End Logo Header -->
  <!-- Navbar Header -->
  <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

    
    <div class="container-fluid"> 
      <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
         @if(Session::has('success'))
      <div class="alert alert-danger text-center alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Great!</strong> {{ Session::get('success') }}
      </div>
      @endif
        <!-- <li class="nav-item toggle-nav-search hidden-caret">
          <a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
            <i class="fa fa-search"></i>
          </a>
        </li> -->


        <li class="nav-item dropdown hidden-caret">
          <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
            <div class="avatar-sm">
              @if(file_exists(Auth::guard('admin')->user()->profile_picture))
              <img src="{{url(Auth::guard('admin')->user()->profile_picture)}}" alt="..." class="avatar-img rounded-circle">
              @else
              <img src="{{ url('public/admin/img/default-user.png') }}" alt="..." class="avatar-img rounded-circle">
              @endif
            </div>
          </a>
          <ul class="dropdown-menu dropdown-user animated fadeIn">
            <div class="dropdown-user-scroll scrollbar-outer">
              <li>
                <div class="user-box">
                  <div class="avatar-lg">
                    @if (file_exists(Auth::guard('admin')->user()->profile_picture))
                    <img src="{{url(Auth::guard('admin')->user()->profile_picture)}}" alt="..." class="avatar-img rounded">
                    @else
                    <img src="{{ url('public/admin/img/default-user.png') }}" alt="..." class="avatar-img rounded">
                    @endif
                  </div>
                  <div class="u-text">
                    <h4>{{Auth::guard('admin')->user()->name}}</h4>
                    <p class="text-muted">{{Auth::guard('admin')->user()->email}}</p>  
                  </div>
                </div>
              </li>
              <li>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{route('admin.profile')}}">My Profile</a> 
                <a class="dropdown-item" href="{{route('admin.change-password')}}">Change Password</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{route('admin.app-setting')}}">App Settings</a>
                <a class="dropdown-item" href="{{route('admin.settings.index')}}">Site Settings</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{route('admin.logout')}}">Logout</a>
              </li>
            </div>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
  <!-- End Navbar -->
</div>