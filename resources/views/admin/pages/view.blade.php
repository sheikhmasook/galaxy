@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">

                    <hr>
                   <div style="text-align:center">
                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Title</strong>
                                <br>
                                <p class="text-muted">{{@($data['title'])?$data['title']:''}}</p>
                            </div>
                           
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>FAQ Description</strong>
                                <br>
                                <p class="text-muted"><?=$data->description?></p>
                            </div>
                           
                        </div>
            </div>
            <div>

            </div>
        </div>
        <div class="row">
                    <div class="col-md-12">
                    <a class="btn btn-success" href="javascript:void(0)" onclick="goBack()">Go Back</a>
                    </div>
                </div>
    </div>
</div>
</div>
</div>
@endsection
