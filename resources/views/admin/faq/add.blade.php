@extends('layouts.admin.admin')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                {{Form::model($data,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off'))}}
                 <div class="form-group">
                    <label>Faq Tttle</label>
                    {{Form::text('title',null,array('placeholder'=>'Page title','id'=>'title','class'=>'form-control'))}}
                </div>
                
                  <div class="form-group">
                    <label>Description</label>
                    {{Form::textarea('description',null,array('placeholder'=>'description Slug','id'=>'description','class'=>'form-control'))}}
                </div>
              </div>
            </div>
                <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button>
                <a class="btn btn-secondary" href="{{route('admin.faq.index')}}">Back</a>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('styles')
<link rel="stylesheet" href="{{url('public/assets/js/plugin/summernote/summernote-bs4.css')}}">
@endsection
@section('scripts')
<script src="{{url('public/assets/js/plugin/summernote/summernote-bs4.min.js')}}"></script>
<script>
$(function(){
  $('#submit-form').ajaxForm({
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true);
    },
    error:function(err){
      handleError(err);
      disable("#submit-btn",false);
    },
    success:function(response){
      disable("#submit-btn",false);
      if(response.status=='true'){
        window.location.href = '{{route('admin.faq.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  });
   $('#description').summernote({
        height:300
    });
});

</script>
@endsection