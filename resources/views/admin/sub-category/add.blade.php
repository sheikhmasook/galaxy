@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
              @if(isset($getcategory))
                {!! Form::model($getcategory,array('class'=>'','id'=>'submit-form','autocomplete'=>'off')) !!}
                @else
                 {!! Form::open(['class'=>'','id'=>'submit-form','autocomplete'=>'off']) !!}
                @endif
                <div class=" p-4">
                   <div class="form-row">

                  <div class="form-group col-12">
                    <label>Category<em style="color:red">*</em></label>
                    {{Form::select('category_id',$category_list,null,array('placeholder'=>'Select Category','id'=>'category_id','class'=>'form-control'))}}
                     <span class=" text-danger error category_id">{{ $errors->first('category_id')}}</span>
                </div> 

                <div class="form-group col-12">
                    <label>Title<em style="color:red">*</em></label>
                    {{Form::text('title',null,array('placeholder'=>'Title','id'=>'title','class'=>'form-control'))}}
                     <span class=" text-danger error title">{{ $errors->first('title')}}</span>
                </div> 
                
                <div class="form-group col-12">
                    <label>Description</label>
                    {!! Form::textarea('description',null,array('placeholder'=>'Description','id'=>'description','class'=>'form-control')) !!}
                     <span class=" text-danger error description">{{ $errors->first('description')}}</span>
                </div> 
                
              </div>
            </div>
                
                {!! Form::button('Save',['class'=>'btn btn-primary set_icon','id'=>'submit-btn','type'=>'submit']) !!}
               
                <a class="btn btn-secondary" href="{{route('admin.category.index')}}">Back</a> 
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$(function(){
   $(document).ready(function() {
  $('#description').summernote();
});
  $('#submit-form').ajaxForm({
   // $(".set_icon").html('<i class="fas fa-spinner fa-spin"></i>');
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
     // alert(response.status)
      if(response.status=='true'){
        window.location.href = '{{route('admin.sub-category.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 
});
</script>
@endsection
