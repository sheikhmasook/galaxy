@extends('layouts.admin.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive">
                        <table id="users-table" class="display table table-striped table-hover w-100" >
                            <thead>
                                <th>Id</th>
                                <th>Category</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Created on</th>
                                <th width="100">Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.sub-category.datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'cat_title', name: 'cat_title'},
            {data: 'title', name: 'title'},
            {data: 'status', name: 'status',orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });
 function deleteuser(id){
      if(id != ""){
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this record!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

            $.ajax({
              url: '{{route('admin.sub-category.delete')}}',
              type: 'POST',
              data:{id:id},
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function (response) {
                swal("Poof! Your sub-category record has been deleted!", {
                  icon: "success",
                });
                $("#delete_"+id).parents("tr").remove();

              }

            });
          }else {
            swal("Your sub-category record is safe!");
          }
        });
      }
    }
</script>
@endsection