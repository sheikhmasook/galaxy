<!DOCTYPE html>
<html lang="en">


<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Login</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <link rel="icon" href="{{ url('public/Admin/img/logo.png')}}" type="image/gif" sizes="32x32">
  <script src="{{url('public/Admin/js/plugin/webfont/webfont.min.js')}}"></script>
  <script>
    WebFont.load({
      google: {"families":["Lato:300,400,700,900"]},
      custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['Admin/css/fonts.min.css']},
      active: function() {
        sessionStorage.fonts = true;
      }
    });
  </script>

  <!-- CSS Files -->
  <link rel="stylesheet" href="{{url('public/Admin/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('public/Admin/css/atlantis.css')}}">
  @yield('styles')
</head>
<body class="login">
  @yield('content')
  <script src="{{url('public/Admin/js/core/jquery.3.2.1.min.js')}}"></script>
  <script src="{{url('public/Admin/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
  <script src="{{url('public/Admin/js/core/popper.min.js')}}"></script>
  <script src="{{url('public/Admin/js/core/bootstrap.min.js')}}"></script>
  <script src="{{url('public/Admin/js/atlantis.min.js')}}"></script>
  @yield('scripts')
</body>


</html>