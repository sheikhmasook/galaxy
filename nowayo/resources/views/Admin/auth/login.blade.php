@extends('layouts.admin.login')
@section('content')
<div class="wrapper wrapper-login wrapper-login-full p-0">
  <div class="login-aside w-50 d-flex flex-column align-items-center justify-content-center text-center bg-secondary-gradient">
     <img src="{{ url('public/Admin/img/logo.png')}}" alt="Admin Logo" class="navbar-brand">
   <!--  <h1 class="title fw-bold text-white mb-3">No Wayo</h1> -->
   <!--  <p class="subtitle text-white op-7">No Wayo</p> -->
  </div>
  <div class="login-aside w-50 d-flex align-items-center justify-content-center bg-white">
    <div class="container container-login container-transparent animated fadeIn">
      @if(Session::has('danger'))
      <div class="alert alert-danger text-center alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Opps!</strong> {{ Session::get('danger') }}
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-danger text-center alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Great!</strong> {{ Session::get('success') }}
      </div>
      @endif
      <h3 class="text-center">Sign In To Admin</h3>
      {!! Form::open() !!}
      <input type="hidden" name="_method" value="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="login-form">
        <div class="form-group">
          <label for="username" class="placeholder"><b>Username</b></label>
          <?=Form::text('email', null, ['class' => 'form-control', 'maxlength' => '255', 'autofocus' => 'autofocus', 'placeholder' => 'Email', 'id' => 'inputEmail', 'required' => true])?>
        </div>
        <div class="form-group">
          <label for="password" class="placeholder"><b>Password</b></label>
          <div class="position-relative">
            <?=Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'inputPassword', 'required' => true]);?>
            <div class="show-password">
              <i class="icon-eye"></i>
            </div>
          </div>
        </div>
        <div class="form-group form-action-d-flex mb-3">
          <button type="submit" class="btn btn-secondary col-md-5 float-right mt-3 mt-sm-0 fw-bold">Sign In</button>
        </div>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
@endsection