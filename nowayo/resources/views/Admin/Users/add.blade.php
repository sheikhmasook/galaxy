<?php 
$gender_list = Config::get('params.gender_list');
?>
@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
                {!! Form::model($getuser,array('files'=>'true','class'=>'','id'=>'registerForm','autocomplete'=>'off')) !!}
                <div class=" p-4">
                   <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Name</label>
                    {{Form::text('name',null,array('placeholder'=>'Name','id'=>'name','class'=>'form-control'))}}
                     <span class=" text-danger error name">{{ $errors->first('name')}}</span>
                </div> 
                
                <div class="form-group col-md-6">
                    <label>Email</label>
                    {!! Form::text('email',null,array('placeholder'=>'Email','id'=>'email','class'=>'form-control')) !!}
                     <span class=" text-danger error email">{{ $errors->first('email')}}</span>
                </div> 
               
                <div class="form-group col-md-6">
                    <label>Phone Number</label>
                    {!! Form::text('phone_no',null,array('placeholder'=>'Phone Number','id'=>'phone_no','class'=>'form-control')) !!}
                    <span class=" text-danger error phone_no">{{ $errors->first('phone_no')}}</span>
                </div> 
                
                <div class="form-group col-md-6">
                    <label>Gender</label>
                    {!! Form::select('gender',$gender_list,null,array('placeholder'=>'Select Gender Number','id'=>'gender','class'=>'form-control')) !!}
                    <span class=" text-danger error gender">{{ $errors->first('gender')}}</span>
                </div> 
                 <!--  @if(empty($id))
                 <div class="form-group col-md-6">
                    <label>Password</label>
                    {!! Form::password('password',array('placeholder'=>'Password Number','id'=>'password','class'=>'form-control')) !!}
                    <span class=" text-danger error gender">{{ $errors->first('password')}}</span>
                </div> 
                 <div class="form-group col-md-6">
                    <label>Confirm Password</label>
                   {!! Form::password('confirm_password',array('placeholder'=>'Password Number','id'=>'confirm_password','class'=>'form-control')) !!}
                    <span class=" text-danger error gender">{{ $errors->first('confirm_password')}}</span>
                </div> 
                @endif -->
                <div class="form-group col-md-12">
                    <label>Image</label>
                    @if($id)
                    <input type="file" id="profile_picture" data-default-file="{{($getuser->profile_picture && file_exists($getuser->profile_picture))?url($getuser->profile_picture):''}}" name="image" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif"/>
                    @else
                    <input type="file" id="image" name="profile_picture" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif"/>
                    @endif
                    <span class=" text-danger error profile_picture">{{ $errors->first('profile_picture')}}</span>
                </div> 
                
              </div>
            </div>
                
                {!! Form::button('Save',['class'=>'btn btn-primary','id'=>'submit-btn','type'=>'submit']) !!}
               
                <a class="btn btn-secondary" href="{{route('admin.users.index')}}">Back</a> 
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$("#registerForm").on('submit',function(e){
    e.preventDefault();
    $("#submit-btn").html('<i class="fas fa-spinner fa-spin"></i>');
    $(".error").remove();
        $.ajax({
            url: "{{route('admin.users.add',$id)}}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
              $("#submit-btn").html('Save');
              if(data.status=='true'){
                  window.location.href = "{{route('admin.users.index')}}";
              }else{
                Alert(data.message,false);
              }
            },
            error: function (data) {
             $("#submit-btn").html('Save');
                if(data.status==500){
                  Alert(data.message,false);
                }
              var response = JSON.parse(data.responseText);
              $.each(response.errors, function (k, v) {
                  $("#" + k).after("<span class='text-danger error'>"+v+"</span>");
              });
            }
        });
  });
</script>
@endsection
