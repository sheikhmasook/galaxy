@extends('layouts.admin.admin')
@section('content')
<!-- <div class="row justify-content-center"> -->
<div class="content">
     @if(Session::has('danger'))
      <div class="alert alert-danger text-center alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Opps!</strong> {{ Session::get('danger') }}
      </div>
       @elseif(Session::has('success'))
      <div class="alert alert-danger text-center alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Great!</strong> {{ Session::get('success') }}
      </div>
      @endif
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => ['admin.change-password'], 'method' => 'post', 'files'=>'true']) !!}
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-default">
                        <label for="inputEmail">Old Password</label>
                        <?=  Form::password('old_password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'inputEmail','required'=>true]); ?>
                    </div>
                    <span class="error">{{ $errors->first('old_password')}}</span>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-default">
                        <label for="inputEmail">Password</label>
                        <?=  Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'inputEmail','required'=>true]); ?>
                    </div>
                      <span class="error">{{ $errors->first('password')}}</span>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group form-group-default">
                        <label for="inputName">Confirm Password</label>
                        <?=  Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password', 'id' => 'inputEmail','required'=>true]); ?>
                    </div>
                    <span class="error">{{ $errors->first('password_confirmation')}}</span>
                </div> 
                <div class="col-lg-12" style="margin: 1% 0;"></div>
                <div class="col-sm-2">
                    <?= Form::submit('Update', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
                </div>
            </div>
            <!-- .row -->
            {{ Form::close() }}
        </div>
        <!-- .card-body -->
    </div>
    <!-- .card -->
    
</div>
<!-- </div> -->
@endsection