@extends('layouts.admin.admin')
@section('content')
<div class="row">
  <div class="col-sm-6 col-md-3">
    <div class="card card-stats card-round">
      <div class="card-body ">
        <div class="row align-items-center">
          <div class="col-icon">
            <div class="icon-big text-center icon-primary bubble-shadow-small">
              <i class="flaticon-users"></i>
            </div>
          </div>
          <div class="col col-stats ml-3 ml-sm-0">
            <div class="numbers">
              <p class="card-category">Total Users</p>
              <h4 class="card-title">{{ $total_users }}</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-3">
    <div class="card card-stats card-round">
      <div class="card-body ">
        <div class="row align-items-center">
          <div class="col-icon">
            <div class="icon-big text-center icon-primary bubble-shadow-small">
              <i class="flaticon-list"></i>
            </div>
          </div>
          <div class="col col-stats ml-3 ml-sm-0">
            <div class="numbers">
              <p class="card-category">Active Users</p>
              <h4 class="card-title">{{ $active_users }}</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection
@section('scripts')
<script src="{{url('public/admin/js/setting-demo.js')}}"></script>
<script src="{{url('public/admin/js/demo.js')}}"></script>
@endsection