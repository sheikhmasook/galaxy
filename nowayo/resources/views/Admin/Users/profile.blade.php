@extends('layouts.admin.admin')
@section('content')
<!-- <div class="row justify-content-center"> -->
<div class="content">
    <div class="card">
        @if(Session::has('danger'))
      <div class="alert alert-danger text-center alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Opps!</strong> {{ Session::get('danger') }}
      </div>
       @elseif(Session::has('success'))
      <div class="alert alert-danger text-center alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Great!</strong> {{ Session::get('success') }}
      </div>
      @endif
        <div class="card-body">
            
            <div class="container-fluid">
                @if(isset($user))
                {{ Form::model($user, ['route' => ['admin.profile'], 'method' => 'post', 'files'=>'true']) }}
                @endif

                
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-default">
                            <label for="inputFullname">Name</label>
                            {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => '255', 'autofocus' => 'autofocus', 'placeholder' => 'Full Name', 'id' => 'inputFullname','required'=>true]) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-group-default">
                            <label for="inputEmail">Email address</label>
                           {!! Form::text('email', null, ['class' => 'form-control', 'maxlength' => '255', 'autofocus' => 'autofocus', 'placeholder' => 'Email', 'id' => 'inputEmail','required'=>true]) !!}
                        </div>
                    </div> 
                    
                    <div class="col-sm-4">
                        <div class="form-group form-group-default">
                            <label for="inputPhone">Profile Picture</label>
                            @if (isset($user->profile_picture) && file_exists($user->profile_picture))
                            <img src="{{ url($user->profile_picture) }}" class="img-thumbnail" alt="<?= $user->name ?>" name="profile_picture" width="150" height="150">
                            @else
                            <img src="{{ url('public/img/default-user.png') }}" class="img-thumbnail" alt="<?= $user->name ?>" name="profile_picture" width="150" height="150">
                            @endif
                            <?= Form::file('profile_picture'); ?>
                        </div>
                    </div>   
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <?= Form::submit('Update', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
                    </div>
                </div>
                <!-- .row -->
                {{ Form::close() }}
            </div>
            <!-- .container-fluid -->
        </div>
        <!-- .card-body -->
    </div>
    <!-- .card -->
    
</div>
@endsection