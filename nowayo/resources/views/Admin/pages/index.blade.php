@extends('layouts.admin.admin')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="text-right mb-3">
                    <a href="{{route('admin.page.add')}}" class="btn btn-secondary">Add New</a>
                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table id="users-table" class="display table table-striped table-hover w-100" >
                            <thead>
                                <th>id</th>
                                <th>Page Title</th>
                                <th>Page Slug</th>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
     $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.page.datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'slug', name: 'slug'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
</script>
@endsection