@extends('layouts.admin.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                 <div class="alert alert-primary">
                    <strong>How To Use:</strong>
                    <p class="m-b-0">You can get the value of each setting anywhere on your site by calling <code>Template('field name')</code></p>
                </div> 
                <div class="row">
                    <div class="table-responsive">
                        <table id="template-table" class="display table table-striped table-hover w-100" >
                            <thead>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Subject</th>
                                 <th>Type</th>
                                 <th>Created on</th>
                                <th>Status</th> 
                                <th width="100">Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('#template-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.template.datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'subject', name: 'subject'},
            {data: 'type', name: 'type'},
            {data: 'created_at', name: 'created_at'},
            {data: 'status', name: 'status',orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']]
    });
</script>
@endsection