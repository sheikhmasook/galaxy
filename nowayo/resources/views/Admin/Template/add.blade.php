<?php 
$gender_list = Config::get('params.gender_list');
?>
@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
                {!! Form::model($getTemplate,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off')) !!}
                <div class="form-group">
                    <label>Title</label>
                    {{Form::text('title',null,array('placeholder'=>'Title','id'=>'title','class'=>'form-control'))}}
                     <span class=" text-danger error title">{{ $errors->first('title')}}</span>
                </div> 
                
                <div class="form-group">
                    <label>Subject</label>
                    {!! Form::text('subject',null,array('placeholder'=>'Subject','id'=>'subject','class'=>'form-control ')) !!}
                     <span class=" text-danger error subject">{{ $errors->first('subject')}}</span>
                </div> 
                <div class="form-group">
                    <label>Keyword</label>
                    {!! Form::text('keyword',null,array('placeholder'=>'Keyword','id'=>'keyword','class'=>'form-control ')) !!}
                     <span class=" text-danger error keyword">{{ $errors->first('keyword')}}</span>
                </div> 
               
                <div class="form-group">
                    <label>Content</label>
                    {!! Form::textarea('content',null,array('placeholder'=>'Enter Content Number','id'=>'content','class'=>'form-control')) !!}
                    <span class=" text-danger error content">{{ $errors->first('content')}}</span>
                </div> 
                 @if($id)
                {!! Form::button('Update',['class'=>'btn btn-primary','id'=>'submit-btn','type'=>'submit']) !!}
                @else
                {!! Form::button('Save',['class'=>'btn btn-primary','id'=>'submit-btn','type'=>'submit']) !!}
                @endif
                <a class="btn btn-secondary" href="{{route('admin.users.index')}}">Back</a> 
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function() {
  $('#content').summernote();
});
$("#submit-form").on('submit',function(e){
    e.preventDefault();
    $("#submit-btn").html('<i class="fas fa-spinner fa-spin"></i>');
    $(".error").remove();
        $.ajax({
            url: "{{route('admin.template.add',$id)}}",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
              $("#submit-btn").html('Save');
              if(data.status=='true'){
                  window.location.href = "{{route('admin.template.index')}}";
              }else{
                Alert(data.message,false);
              }
            },
            error: function (data) {
             $("#submit-btn").html('Save');
                if(data.status==500){
                  Alert(data.message,false);
                }
              var response = JSON.parse(data.responseText);
              $.each(response.errors, function (k, v) {
                  $("#" + k).after("<span class='text-danger error'>"+v+"</span>");
              });
            }
        });
  });
</script>
@endsection
