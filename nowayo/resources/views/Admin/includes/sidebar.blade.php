<?php
$action = app('request')->route()->getAction();
$controller = class_basename($action['controller']);
// print_r($controller);
// print_r($action);
list($controller, $action) = explode('@', $controller);
?>
<div class="sidebar sidebar-style-2">
  <div class="sidebar-wrapper scrollbar scrollbar-inner">
    <div class="sidebar-content">
      <div class="user">
        @if (isset(Auth::guard('admin')->user()->profile_picture) && file_exists(Auth::guard('admin')->user()->profile_picture))
        <div class="avatar-sm float-left mr-2">
          <img src="{{url(Auth::guard('admin')->user()->profile_picture)}}" alt="..." class="avatar-img rounded-circle">
        </div>
        @else
        <div class="avatar-sm float-left mr-2">
          <img src="{{ url('public/admin/img/default-user.png') }}" alt="..." class="avatar-img rounded-circle">
        </div>
        @endif
        <div class="info">
          <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
            <span>
              <?php echo (Auth::guard('admin')->user()->name); ?>
              <span class="user-level">Administrator</span>
              <span class="caret"></span>
            </span>
          </a>
          <div class="clearfix"></div>
          <div class="collapse in" id="collapseExample">
            <ul class="nav">
              <li>
                <a href="{{route('admin.profile')}}">
                  <span class="link-collapse">My Profile</span>
                </a>
              </li>
              <li>
                <a href="{{route('admin.logout')}}">
                  <span class="link-collapse">Logout</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <ul class="nav nav-primary">
        <?php $active = ($controller == "UsersController" && in_array($action, ['dashboard'])) ? "active" : "";?>
        <li class="nav-item {{$active}}">
          <a href="{{route('admin.dashboard')}}">
            <i class="fas fa-tachometer-alt"></i>
            <p>Dashboard</p>
          </a>
        </li>

       

       <!-- users Management -->
        <?php $active = ($controller == "UsersController" && in_array($action, ['index','add','view'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#users">
            <i class="fas fa-user"></i>
            <p>Users Management</p>
            <span class="caret"></span>
          </a>
          <?php $div_active = ($controller == "UsersController" && in_array($action, ['index','add','view'])) ? "show" : "";?>
          <div class="collapse {{ $div_active }}" id="users">
            <ul class="nav nav-collapse">
            <?php $li_active = ($controller == "UsersController" && in_array($action, ['index','view'])) ? "active" : "";?>
               <li class="{{ $li_active }}">
                <a href="{{ route('admin.users.index') }}">
                  <span class="sub-item">List of Users</span>
                </a>
              </li>
               <?php $li_active2 = ($controller == "UsersController" && in_array($action, ['add'])) ? "active" : "";?>
              <li class="{{ $li_active2 }}">
                <a href="{{ route('admin.users.add') }}">
                  <span class="sub-item">Create User</span>
                </a>
              </li>
             
            </ul>
          </div>
        </li>

        <!-- template management -->
        <?php $active = ($controller == "TemplateController" && in_array($action, ['index','add','view'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#template">
            <i class="fas fa-envelope"></i>
            <p>Template Management</p>
            <span class="caret"></span>
          </a>
          <?php $div_active = ($controller == "TemplateController" && in_array($action, ['index','add'])) ? "show" : "";?>
          <div class="collapse {{ $div_active }}" id="template">
            <ul class="nav nav-collapse">
            <?php $li_active = ($controller == "TemplateController" && in_array($action, ['index','view'])) ? "active" : "";?>
               <li class="{{ $li_active }}">
                <a href="{{ route('admin.template.index') }}">
                  <span class="sub-item">List of Template</span>
                </a>
              </li>
               <?php $li_active2 = ($controller == "TemplateController" && in_array($action, ['add'])) ? "active" : "";?>
              <li class="{{ $li_active2 }}">
                <a href="{{ route('admin.template.add') }}">
                  <span class="sub-item">Create Template</span>
                </a>
              </li>
             
            </ul>
          </div>
        </li>

         <!-- CMS module -->

        <?php $active = ($controller == "PageController" && in_array($action, ['index','add','view'])) ? "active" : "";?>
        <li class="nav-item {{$active}}">
          <a href="{{route('admin.page.index')}}">
            <i class="fas fa-file"></i>
            <p>CMS Management</p>
          </a>
        </li>


        <!-- setting management -->
        <?php $active = ($controller == "SettingController" && in_array($action, ['index','add','view'])) ? "active submenu" : "";?>
        <li class="nav-item {{$active}}">
          <a data-toggle="collapse" href="#setting">
            <i class="fas fa-cog"></i>
            <p>Setting Management</p>
            <span class="caret"></span>
          </a>
          <?php $div_active = ($controller == "SettingController" && in_array($action, ['index','add'])) ? "show" : "";?>
          <div class="collapse {{ $div_active }}" id="setting">
            <ul class="nav nav-collapse">
            <?php $li_active = ($controller == "SettingController" && in_array($action, ['index','view'])) ? "active" : "";?>
               <li class="{{ $li_active }}">
                <a href="{{ route('admin.settings.index') }}">
                  <span class="sub-item">Site Setting</span>
                </a>
              </li>

            </ul>
          </div>
        </li>
         

      </ul>
    </div>
  </div>
</div>