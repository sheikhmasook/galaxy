<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization, X-CSRF-Token');
header('Access-Control-Allow-Credentials: true');

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::group(['namespace' => 'Api'], function () {
	//customer user managment routes
	Route::post('country-list', [UserController::class, 'countryList']);
	Route::post('user-login', [UserController::class, 'login']);
	Route::post('get-static-page', [UserController::class, 'getStaticPage']);
	Route::post('check-user', [UserController::class, 'checkUser']);
	Route::post('user-register', [UserController::class, 'register']);
	
});

Route::group(['namespace' => 'Api', 'middleware' => ['jwt.auth']], function () {
	Route::post('update-profile', [UserController::class, 'updateProfile']);
	Route::post('notification-list', [UserController::class, 'notificationList']);
	Route::post('notification-delete', [UserController::class, 'notificationDelete']);
	Route::post('logout', [UserController::class, 'logout']);

});