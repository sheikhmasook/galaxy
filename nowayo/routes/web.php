<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\TemplateController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\PagesController;
use App\Http\Controllers\Admin\PageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

/*-------------------------front routes-----------------------*/
Route::group(['namespace' => 'Front'], function () {
Route::get('/', [HomeController::class, 'index'])->name('front.home.index');
Route::get('/page/{slug}', [PagesController::class, 'AppPages']);
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
	$exitCode = Artisan::call('config:cache');
	echo "cache alera";die;
    // return what you want
});
});
/*-------------------------admin routes-----------------------*/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
	Route::any('/', [LoginController::class, 'login'])->name('admin.login');
	Route::any('/login', [LoginController::class, 'login'])->name('admin.login');
	Route::middleware(['admin'])->group(function () {
			Route::any('/dashboard', [UsersController::class, 'dashboard'])->name('admin.dashboard');
			// user route
			Route::any('users', [UsersController::class, 'index'])->name('admin.users.index');
			Route::any('users/datatables', [UsersController::class, 'datatables'])->name('admin.users.datatables');
			Route::any('/user/add/{id?}', [UsersController::class, 'add'])->name('admin.users.add');
			Route::any('/user/view/{id}', [UsersController::class, 'view'])->name('admin.users.view');
			Route::any('/user/status', [UsersController::class, 'status'])->name('admin.users.status');
			Route::any('/user/delete', [UsersController::class, 'delete'])->name('admin.users.delete');
			Route::any('/user/profile', [UsersController::class, 'profile'])->name('admin.profile');
			Route::any('/user/change-password', [UsersController::class, 'changePassword'])->name('admin.change-password');
			//  template route

			Route::any('template', [TemplateController::class, 'index'])->name('admin.template.index');
			Route::any('template/datatables', [TemplateController::class, 'datatables'])->name('admin.template.datatables');
			Route::any('template/add/{id?}', [TemplateController::class, 'add'])->name('admin.template.add');
			Route::any('template/view/{id}', [TemplateController::class, 'view'])->name('admin.template.view');
			Route::any('template/status', [TemplateController::class, 'status'])->name('admin.template.status');

			// page routes
			Route::any('page', [PageController::class, 'index'])->name('admin.page.index');
			Route::any('page/datatables', [PageController::class, 'datatables'])->name('admin.page.datatables');
			Route::any('/page/add/{id?}', [PageController::class, 'add'])->name('admin.page.add');
			Route::any('/page/view/{id}', [PageController::class, 'view'])->name('admin.page.view');
			Route::any('/page/status', [PageController::class, 'status'])->name('admin.page.status');
			Route::any('/page/delete/{id?}', [PageController::class, 'delete'])->name('admin.page.delete');
			// site setting
			Route::any('setting', [SettingController::class, 'index'])->name('admin.settings.index');
			Route::any('setting/datatables', [SettingController::class, 'datatables'])->name('admin.settings.datatables');
			Route::any('setting/add/{id?}', [SettingController::class, 'add'])->name('admin.settings.add');
			Route::any('setting/view/{id}', [SettingController::class, 'view'])->name('admin.settings.view');
			Route::any('setting/status', [SettingController::class, 'status'])->name('admin.settings.status');


			Route::any('/site-setting', [UsersController::class, 'profile'])->name('admin.site-setting');
			Route::any('/app-setting', [UsersController::class, 'profile'])->name('admin.app-setting');
			Route::any('/logout', [LoginController::class, 'logout'])->name('admin.logout');

		});
});


