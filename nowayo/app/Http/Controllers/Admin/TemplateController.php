<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Template;
use DataTables;
use App\Lib\Helper;
use Illuminate\Support\Facades\Validator;

class TemplateController extends Controller
{
    public function index(){
      $page_title = 'Template List';
      $breadcrumbs = [
                ['name'=>'Template','relation'=>'Current','url'=>'']
            ];
   	 return view('Admin.Template.index',compact('page_title','breadcrumbs'));

    }
    public function add(Request $request,$id=null){
       
       $getTemplate = Template::find($id);
       if($getTemplate){
        $page_title = 'Edit Template';
         $breadcrumbs = [
                ['name'=>'Template','relation'=>'link','url'=>route('admin.template.index')],
                ['name'=>'Edit Template','relation'=>'Current','url'=>'']
            ];
          }else{
            $page_title = 'Add Template';
             $breadcrumbs = [
                ['name'=>'Template','relation'=>'link','url'=>route('admin.template.index')],
                ['name'=>'Add Template','relation'=>'Current','url'=>'']
            ];
          }
       
       if($request->isMethod('post')){
        $postdata  = $request->all();
         $rules = [
          'title' => 'required',
          'subject' => 'required',
          'keyword' => 'required',
          'content' => 'required',
       ];
        $validator = Validator::make($postdata,$rules);
          if ($validator->fails()) {
            return response()->json(array('errors' => $validator->messages()), 422);
          }else{ 
              if($id){
                 $getTemplate->update($postdata);
                    Session::flash('success','Template Update Successfully');
              return ['status' => 'true', 'message' => 'Template Update Successfully.'];
              }else{
                Template::create($postdata);
                Session::flash('success','Template Create Successfully');
                return ['status' => 'true', 'message' => 'Template Create Successfully.'];
              }
              
          }
        
      }
       return view('Admin.Template.add',compact('getTemplate','page_title','id','breadcrumbs'));
   

    }

     public function datatables() {
      $templates = Template::all();
    return DataTables::of($templates)
      ->addColumn('action', function ($template) {
        return '<a title="Edit" href="' . route('admin.template.add', $template->id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"> Edit</i></a>';
      })
      ->editColumn('type', function ($template) {
        return ($template->type == 1) ? "SMS" : "Email";
      })
      ->editColumn('status', function ($template) {
        return Helper::getStatus($template->status, $template->id, route('admin.template.status'));
      })
      ->editColumn('created_at', function ($template) {
        return date("d-M-Y", strtotime($template->created_at));
      })
      ->rawColumns(['status', 'action','type','created_at'])
      ->make(true);
  }

  public function status(Request $request) {
    $id = $request->id;
    $row = Template::whereId($id)->first();
    $row->status = $row->status == '1' ? '0' : '1';
    $row->save();
    return Helper::getStatus($row->status, $id, route('admin.template.status'));
  }

}
