<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use DataTables;
use App\Lib\Helper;
use Session;

class UsersController extends Controller
{
	public function index(){
        $breadcrumbs = [
                ['name'=>'Users','relation'=>'Current','url'=>'']
            ];
       $page_title = 'Users List';
   	   $allusers = User::where(['status'=>'1','role'=>'user'])->get();
   	 return view('Admin.Users.index',compact('allusers','page_title','breadcrumbs'));
   }
   public function add(Request $request,$id=null){
        $getuser = User::find($id);
       if($getuser){
         $page_title = 'Edit User';
         $breadcrumbs = [
                ['name'=>'Users','relation'=>'link','url'=>route('admin.users.index')],
                ['name'=>'Edit User','relation'=>'Current','url'=>'']
            ];
          }else{
             $page_title = 'Add User';
             $breadcrumbs = [
                ['name'=>'Users','relation'=>'link','url'=>route('admin.users.index')],
                ['name'=>'Add User','relation'=>'Current','url'=>'']
            ];
          }
      
      
       if($request->isMethod('post')){
        $postdata  = $request->all();
         $rules = [
          'name' => 'required',
          'phone_no' => 'required',
          'gender' => 'required|in:male,female',
          'email' => 'required|email|unique:users,email,'.$id,
       ];
        // if($id == null){
        //   $rules['password'] = 'required|min:6';
        //   $rules['confirm_password'] = 'required|same:password|min:6';
        //   $password = Hash::make($postdata['password']); // creating password hash / encryption.
        //   $postdata['password'] = $password;
        // }
        $validator = Validator::make($postdata,$rules);
          if ($validator->fails()) {
           return response()->json(array('errors' => $validator->messages()), 422);
          }else{ 
               $postdata['register_with'] = 'ADMIN';
              if($request->hasFile('profile_picture')){
                  $img = $request->file('profile_picture');
                  $file_ext = $img->getClientOriginalExtension();
                  $filename = time() . '.' . $file_ext;
                  $destination_path = 'uploads/users/'.$filename;
                    if(Storage::disk('public')-> put($destination_path, file_get_contents($img->getRealPath()))){
                       $postdata['profile_picture'] = 'public/'.$destination_path;
                    }
               }
              if($id){
                 $getuser->update($postdata);
                  Session::flash('success','Registration Update Successfully');
              return ['status' => 'true', 'message' => 'Registration Update Successfully.'];
              }else{
                User::create($postdata);
                 Session::flash('success','Registration Create Successfully');
                return ['status' => 'true', 'message' => 'Registration Create Successfully.'];
              }
              
          }
        
      }
       return view('Admin.Users.add',compact('getuser','page_title','id','breadcrumbs'));
   }
   public function dashboard(){
      $page_title = 'Dashboard';
       // total users
   	 $total_users = User::where(['role'=>'user'])->count();
       // active users
       $active_users = User::where(['status'=>'1','role'=>'user'])->count();
       // new users
       $new_users = User::where(['status'=>'1','role'=>'user','created_at'=>date('Y-m-d')])->count();
   	 return view('Admin.Users.dashboard',compact('total_users','page_title','active_users','new_users'));
   }
   public function profile(Request $request){
       $page_title = 'Admin Profile';
       $id = Auth::guard('admin')->user()->id;
       $user = User::find($id);
       if($request->isMethod('post')){
         $postdata = $request->all();
         if($request->hasFile('profile_picture')){
            $img = $request->file('profile_picture');
            $file_ext = $img->getClientOriginalExtension();
            $filename = time() . '.' . $file_ext;
            $destination_path = 'uploads/users/'.$filename;
            if(Storage::disk('public') -> put($destination_path, file_get_contents($img ->getRealPath()))){
               $postdata['profile_picture'] = 'public/'.$destination_path;
            }
         }
          $user->update($postdata);
          Session::flash('success','Profile updated Successfully');
         return redirect()->route('admin.profile');
       }
   	 return view('Admin.Users.profile',compact('page_title','user'));
   }
   public function datatables() {
    $users = User::where(['role'=>'user'])
            ->select(['id', 'name', 'email','phone_no',  'status', 'created_at', 'register_with'])
            ->orderBy('id','desc')
            ->get();

    return DataTables::of($users)
      ->addColumn('action', function ($user) {
        return '<a title="Edit" href="' . route('admin.users.add', $user->id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>&nbsp;<a title="View" href="' . route('admin.users.view', $user->id) . '" class="btn btn-xs btn-primary"><i class="fas fa-eye"></i></a>&nbsp;<a title="Delete" data-link="' . route('admin.users.delete') . '" id="delete_' . $user->id . '" onclick="deleteuser(' . $user->id . ')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i></a>';
      })
     
      ->editColumn('status', function ($user) {
        return Helper::getStatus($user->status, $user->id, route('admin.users.status'));
      })
      ->editColumn('created_at', function ($user) {
        return date("d-M-Y", strtotime($user->created_at));
      })
     
      ->rawColumns(['status', 'action'])
      ->make(true);
  }

  public function status(Request $request) {
    $id = $request->id;
    $row = User::whereId($id)->first();
    $row->status = $row->status == '1' ? '0' : '1';
    $row->save();
    return Helper::getStatus($row->status, $id, route('admin.users.status'));
  }

  public function view($id) {
    $page_title = 'View User';
    $breadcrumbs = [
                ['name'=>'Users','relation'=>'link','url'=>route('admin.users.index')],
                ['name'=>'View User','relation'=>'Current','url'=>'']
            ];
    $data = User::where('id', $id)->first();
    if ($data) {
      $page_title = "Profile - " . $data->name;
      return view('Admin.Users.view', compact('page_title', 'data','breadcrumbs'));
    } else {
      return abort(404);
    }
  }
   public function delete(Request $request) {
    $id = $request->id;
    $data = User::findorFail($id)->delete();
    if ($data) {
       return ['status'=>'true','msg'=>'User delete Successfully'];
    } else {
      return abort(404);
    }
  }
    public function changePassword(Request $request){
       $page_title = 'Change Password';
       $id = Auth::guard('admin')->user()->id;
       $user = User::find($id);
      if($request->isMethod('post')){
        $postdata  = $request->all();
        $validator = Validator::make($postdata,[
        'old_password' => 'required',
        'password' => 'required|min:8',
        'password_confirmation' => 'required|same:password|min:6',
       ]);
          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
          }else{
             if(Hash::check($postdata['old_password'], $user->password)) {
              $user->password = Hash::make($postdata['password']);
              $user->update($postdata);
                Session::flash('success','Password Change Successfully');
              return redirect()->route('admin.dashboard');
             }
          }
        
      }
       return view('Admin.Users.change_password',compact('user','page_title'));
   }
}
