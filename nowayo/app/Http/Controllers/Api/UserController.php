<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Country;
use App\Models\UserOtp;
use App\Models\Page;
use App\Models\User;
use App\Models\UserDevices;
use App\Models\Notification;
use App\Lib\Uploader;
use JWTAuth;
use DB;


class UserController extends Controller {

// country list
	public function countryList(){
		try {
			$countries = Country::orderBy('id', 'asc')->get();
			return response()->json(['status' => true, 'message' => 'Success', 'data' => $countries]);
		} catch (\Exception $e) {
			return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
		}

	}

	/* check user */
	public function checkUser(Request $request) {
// Begin Transaction
		DB::beginTransaction();
		try {
			if ($request->isMethod('post')) {
				$data = $request->all();
				$validator = Validator::make($data, [
					'country_code' => 'required',
					'phone_no' => 'required|numeric',
					// 'phone_no' => ['required', 'numeric', 'digits_between:7,15', Rule::unique('users')->where(function ($query) use ($request) {
					// 	$query->where(['country_code' => $request->country_code, 'deleted_at' => null]);
					//})],
				]);

				if ($validator->fails()) {
					$error = $this->validationHandle($validator->messages());
					return response()->json(['status' => false, 'message' => $error]);
				} else {
					$user = User::where(['phone_no' => $data['phone_no'], 'country_code' => $data['country_code']])
					->first();
					if (empty($user)) {
						$isexist = DB::table('user_otps')
						->where(['phone_no' => $data['phone_no'], 'country_code' => $data['country_code']])
						->first();
						if ($isexist == null) {
							$adduser = new UserOtp();
						} else {
							$adduser = UserOtp::find($isexist->id);
						}
						$otp = rand(1000, 9999);
						$adduser->phone_no = $data['phone_no'];
						$adduser->country_code = $data['country_code'];
						$adduser->otp_code = $otp;
						$adduser->save();
						DB::commit();
						$phone_no = $adduser->phone_no;
						$country_code = $adduser->country_code;
						return response()->json(['status' => true, 'message' => "OTP send Successfully", 'data' => ['otp' => $otp,'phone_no'=>$phone_no,'country_code'=>$country_code],'status_code'=>200]);
					} else {
						$otp = rand(1000, 9999);
						$user->otp_code = $otp;
						$user->save();
						DB::commit();
						$phone_no = $user->phone_no;
						$country_code = $user->country_code;
						return response()->json(['status' => true, 'message' => "OTP send Successfully", 'data' => ['otp' => $otp,'phone_no'=>$phone_no,'country_code'=>$country_code],'status_code'=>201]);
					}
				}
			}
		} catch (Exception $e) {
			DB::rollBack();
			return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
		}

	}
// resend OTP
	public function resendOtp(Request $request) {
		try {
			$rules = [
				'country_code' => 'required|numeric',
				'phone_no' => ['required', 'numeric', 'digits_between:7,15', Rule::unique('users')->where(function ($query) use ($request) {
					$query->where(['country_code' => $request->country_code, 'deleted_at' => null]);
				})],
			];

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
// If validation fails.
				$response['status'] = false;
				$response['message'] = $this->validationHandle($validator->messages());
				$response['data'] = [];
				$this->response($response);
				$response = array('status' => false, 'response' => $response);
				return $response;
			} else {
				$otp = UserOtp::where(['phone_no' => $request->phone_no, 'country_code' => $request->country_code])->first();
				if (!$otp) {
					$otp = new UserOtp;
					$otp->country_code = $request->country_code;
					$otp->phone_number = $request->phone_number;
					$otp->otp_code = rand(1000, 9999);
					$otp->save();
					$phone_no = $otp->phone_no;
					$country_code = $otp->country_code;
				}
				return response()->json(['status' => true, 'message' => __("OTP send successfully"), 'data'=>['otp' => $otp,'phone_no'=>$phone_no,'country_code'=>$country_code]]);
			}
		} catch (Exception $e) {
			return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
		}
	}
		/* user register */
	public function register(Request $request) {
		try {
			$data = $request->all();
			$validator = Validator::make($data, [
				'country_code' => 'required',
				'phone_no' => 'required|numeric',	
				'otp' => 'required|numeric',	
				'device_type' => 'required|in:IOS,ANDROID',
				'device_token' => 'required',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => false, 'message' => $error]);
			} else {
				    $conditions = [];
						$conditions['country_code'] = $data['country_code'];
						$conditions['phone_no'] = $data['phone_no'];
						$conditions['otp_code'] = $data['otp'];
				        $user = UserOtp::where($conditions)->first();
						if (!$user) {
							return response()->json(['status' => false, 'message' => __("Invalid Mobile Number or OTP does not match."), 'data' => []]);
						} else {
							if($user){
								DB::beginTransaction();
								$user->delete();
								$create = [
									'country_code' => $data['country_code'],
									'phone_no' => $data['phone_no'],
									'status' => 1,
								];
								//print($create);die;
							   $save_user = User::create($create);
							   $user = User::getProfile($save_user->id);
								if ($user) {
									UserDevices::deviceHandle([
									"id" => $user->id,
									"device_type" => $data['device_type'],
									"device_token" => $data['device_token'],
									]);
								 }
								 // echo '<pre>';
								 // print_r($user); die;
								$user->token = JWTAuth::fromUser($user);
								DB::commit();
							   return response()->json(['status' => true, 'message' => __("User Registration successfully"), 'data' => $user]);
							}else {
								$message = __("Incorrect OTP");
								return response()->json(['status' => false, 'message' => $message, 'data' => []]);
							}
				      }
					}
		} catch (Exception $e) {
			return response()->json(['status' => false, 'message' => $e->getLine() . ' : ' . $e->getMessage(), 'data' => []]);
		}
	}

	/* user login */
	public function login(Request $request) {
		try {
			$data = $request->all();
			$validator = Validator::make($data, [
				'country_code' => 'required',
				'phone_no' => 'required|numeric',	
				'otp' => 'required|numeric',
				'device_type' => 'required|in:IOS,ANDROID',
				'device_token' => 'required',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => false, 'message' => $error]);
			} else {
				$conditions = [];
				$conditions['country_code'] = $data['country_code'];
				$conditions['phone_no'] = $data['phone_no'];
				//$conditions['otp_code'] = $data['otp'];			
				$user = User::where($conditions)->first();
				$user_id = $user->id;
				if (!$user) {
					return response()->json(['status' => false, 'message' => __("Invalid Mobile Number."), 'data' => []]);
				} else {
					if($user->otp_code == $data['otp']){
						if ($user->status == 0) {
							return response()->json(['status' => false, 'message' => __("Your account is inactive, please contact to administrator."), 'data' => []]);
						}else{
							UserDevices::deviceHandle([
								"id" => $user->id,
								"device_type" => $data['device_type'],
								"device_token" => $data['device_token'],
							]);
							$user->otp_code = NULL;
							$user->save();
							$user = User::getProfile($user_id);
							$user->token = JWTAuth::fromUser($user);

							return response()->json(['status' => true, 'message' => __("Login Successfully"), 'data' => $user]);
						}
					}else{
						return response()->json(['status' => false, 'message' => __("OTP does not match."), 'data' => []]);
					}
					
				}

			}
		
		} catch (Exception $e) {
			return response()->json(['status' => false, 'message' => $e->getLine() . ' : ' . $e->getMessage(), 'data' => []]);
		}
	}

	public function updateProfile(Request $request) {
		try {
			$data = $request->all();
			$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
			$validator = Validator::make($request->all(), [
				'name' => 'required|min:2|max:45',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => false, 'message' => $error]);
			} else {
				$user = User::find($user_id);
				$user->name = $data['name'];
				if ($request->file('profile_image') !== null) {
					$destinationPath = '/uploads/profile/';
					$responseData = Uploader::doUpload($request->file('profile_image'), $destinationPath, false);
					if ($responseData['status'] == "true") {
						$user->profile_picture = $responseData['file'];
					}
				}
				if ($user->save()) {
					$user = User::getProfile($user->id);
					return response()->json(['status' => true, 'message' => __("Profile updated successfully"), 'data' => $user]);
				} else {
					return response()->json(['status' => false, 'message' => __("unable to update profile"), 'data' => []]);
				}
			}
		} catch (Exception $e) {
			return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
		}
	}



	public function getStaticPage(Request $request) {
		try {
			$validator = Validator::make($request->all(), [
				'slug' => 'required',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => false, 'message' => $error]);
			} else {
				$slug = $request->get('slug');
				$page = Page::where('slug', $slug)->select('id', 'title', 'description')->first();
				if ($page) {
					return response()->json(['status' => true, 'message' => 'Static page data.', 'data' => $page]);
				} else {
					return response()->json(['status' => false, 'message' => 'Invalid slug passed.', 'data' => []]);
				}
			}
		} catch (\Exception $e) {
			return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
		}
	}
	// Notification list
	public function notificationList(){
		try {
			$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
			$notify_list = Notification::where('user_id',$user_id)
			        ->select(['id','content','created_at'])
			        ->orderBy('id', 'asc')
					->get();
			if(count($notify_list) > 0){
				return response()->json(['status' => true, 'message' => 'Notify list get successfully', 'data' => $notify_list]);
			}else{
				return response()->json(['status' => false, 'message' => 'No record found', 'data' => []]);
			}
		} catch (\Exception $e) {
			return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
		}

	}
	// Notification list
	public function notificationDelete(Request $request){
		try {
			$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
			$validator = Validator::make($request->all(), [
				'id' => 'required',
				'type' => 'required|in:1,2',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => false, 'message' => $error]);
			} else {
				if($request->type == 1){
				   $notify_list = Notification::where(['user_id'=>$user_id,'id'=>$request->id])->delete();
				}elseif($request->type == 2){
					$notify_list = Notification::where(['user_id'=>$user_id,'id'=>$request->id])->delete();
				}else{
				  return response()->json(['status' => false, 'message' => 'Type does not exist']);	
				}	
			if($notify_list){
				return response()->json(['status' => true, 'message' => 'Notify delete successfully']);
			}else{
				return response()->json(['status' => false, 'message' => 'No record found']);
			}
		 }
		} catch (\Exception $e) {
			return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
		}

	}
	public function logout(Request $request) {
		try {
			$validator = Validator::make($request->all(), [
				'device_type' => 'required|in:ANDROID,IOS',
				'device_token' => 'required',
			]);
			if ($validator->fails()) {
				$error = $this->validationHandle($validator->messages());
				return response()->json(['status' => false, 'message' => $error]);
			} else {
				$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
				$userDevice = UserDevices::where(['device_type' => $request['device_type'], 'device_token' => $request['device_token']])->first();
				if ($userDevice) {
					UserDevices::where(['device_type' => $request['device_type'], 'device_token' => $request['device_token']])->delete();
				}
				JWTAuth::invalidate();
				return response()->json(['status' => true, 'message' =>"Logout successfully"]);
			}
		} catch (Exception $e) {
			return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
		}
	}

}