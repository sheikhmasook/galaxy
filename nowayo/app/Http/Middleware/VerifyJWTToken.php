<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $token = JWTAuth::getToken();
            //$user = JWTAuth::toUser($request->input('token'));
            $user = JWTAuth::toUser($token);
            if(empty($user)){
                return response()->json(['status'=>false,'message'=>'User not found']);
            }elseif(empty($user->status)){
                $message = ($user->status == 0) ? 'You have been marked as inactive by the administrator' : 'User not found';
                return response()->json(['status'=>false,'message'=>$message]);
            }
        }catch (JWTException $e) {
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['status'=>false,'message'=>'token_expired']);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['status'=>false,'message'=>'token_invalid']);
            }else{
                return response()->json(['status'=>false,'message'=>'Token is required']);
            }
        }
        
       return $next($request);
    }
}
