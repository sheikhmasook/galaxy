<?php
return [
	'gender_list' => ['male'=>'Male','female'=>'Female','other'=>'Other'],
	'units' => [
			'Litre'=>'Litre',
			'KG'=>'KG',
			'Gram'=>'Gram',
			'Piece'=>'Piece',
 ],
];